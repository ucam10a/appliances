package com.yung.debug;

import java.util.logging.Logger;


public class JavaLogDebugPrinter extends AbstractDebugPrinter {

    private static Logger logger = Logger.getLogger(JavaLogDebugPrinter.class.getName());
    
    @Override
    public void printMessage(String message) {
        logger.info(LINE_SEPARATOR + message);
    }

}