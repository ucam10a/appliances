package com.yung.debug;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a mock service player, the main purpose for this object is to intercept and hijack backend-side SAP/WEB-SERVICE/JAP to
 * 1. RECORD
 *    record all POJO/VO/TO object to json string, json can be encode to base64 to keep Chinese characters
 *    finally, call flush method to get json
 * 2. REPLAY
 *    before you use replay mode, you have to put you encode json string into "mock.properties" under mock.json
 *    then, mock service play will load json then store it for replay
 * 
 * @author Yung Long Li
 *
 */
public class MockServicePlayer {
    
    /**
     * define mock service mode
     * 1. NORMAL: normal node, do nothing special
     * 2. RECORD: record return object to json and then return 
     * 3. REPLAY: call serviceDataMap and get json then convert to object then return
     * 
     * @author Yung Long Li
     *
     */
    public enum Mode {
        RECORDER, REPLAY, NORMAL
    }
    
    public Class<?> debugCls = AppLogger.getDebugCls();
    public String debugMethod = AppLogger.getDebugMethod();
    private String[][] overloadFilters = new String[][]{};
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    /** java logger */
    private static Logger logger = AppLogger.getLogger(MockServicePlayer.class.getName());
    
    /** marshal folder */
    private String marshalDir = "";
    
    /** mock service mode, default is normal */
    private Mode mode = Mode.NORMAL;
    
    public MockServicePlayer(String[][] overloadFilters) {
        this.overloadFilters = overloadFilters;
    }

    public MockServicePlayer() {
    }

    /**
     * set marshal directory
     * 
     * @param marshalDir marshal directory
     */
    public void setMarshalDir(String marshalDir){
        this.marshalDir = marshalDir;
    }
    
    /**
     * get mock service mode 
     * 
     * @return mock service mode
     */
    public Mode getMode() {
        return mode;
    }

    /**
     * set mock service mode
     * 
     * @param _MODE mock service mode
     */
    public void setMode(Mode mode) {
        this.mode = mode;
        if (mode == Mode.RECORDER){
            logger.info("ServiceMock mode is RECORDER, this is not allowed in production");
        } else if (mode == Mode.REPLAY){
            logger.info("ServiceMock mode is REPLAY,  this is not allowed in production");
        }
    }
    
    /**
     * read file to string
     * 
     * @param file file
     * @return string in file
     * @throws IOException
     */
    protected static String readFile(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
    
    /**
     * mock service for object
     * 
     * @param cls result object class type
     * @param result result object
     * @param method method name
     * @param inputs string inputs
     * @return result object
     */
    public Object server(Object result, String method, Object... inputs){
        try {
            if (mode == Mode.NORMAL){
                return result;
            }
            if (mode == Mode.RECORDER){
                record(result, method, inputs);
            } else if (mode == Mode.REPLAY) {
                result = replay(null, method, inputs);
            }
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * mock service for list object
     * 
     * @param cls list type class
     * @param result list result object
     * @param method method name
     * @param inputs method input, please convert all inputs to string, if not, you have define a unique key
     * @return list result
     */
    public List<?> serverList(List<?> result, String method, Object... inputs) {
        try {
            if (mode == Mode.NORMAL){
                return result;
            }
            if (mode == Mode.RECORDER){
                recordList(result, method, inputs);
            } else if (mode == Mode.REPLAY) {
                result = replayList(null, method, inputs);
            }
            return result;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    /**
     * mock service for array
     * 
     * @param result array objects
     * @param method method name
     * @param inputs inputs
     * @return array object
     */
    public Object[] serverArray(Object[] result, String method, Object... inputs) {
        try {
            if (mode == Mode.NORMAL){
                return result;
            }
            if (mode == Mode.RECORDER){
                recordArray(result, method, inputs);
            } else if (mode == Mode.REPLAY) {
                result = replayArray(null, method, inputs);
            }
            return result;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    /**
     * mock service for map object
     * 
     * @param cls list type class
     * @param result list result object
     * @param method method name
     * @param inputs method input, please convert all inputs to string, if not, you have define a unique key
     * @return list result
     */
    public Map<?, ?> serverMap(Map<?, ?> result, String method, Object... inputs) {
        try {
            if (mode == Mode.NORMAL){
                return result;
            }
            if (mode == Mode.RECORDER){
                recordMap(result, method, inputs);
            } else if (mode == Mode.REPLAY) {
                result = replayMap(null, null, method, inputs);
            }
            return result;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    /**
     * record object to marshal file
     * 
     * @param cls object class
     * @param method method name
     * @param inputs method inputs
     * @return method return object
     * @throws IOException 
     * @throws NoSuchAlgorithmException 
     */
    public Object record(Object result, String method, Object... inputs) throws IOException, NoSuchAlgorithmException{
        String key = generateKey(method, inputs, true);
        if ((result != null && isDebug(result.getClass(), method))) {
            logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
        }
        if (result == null){
            logger.log(Level.FINE, "result is null");
            return result;
        }
        MarshalHelper helper = new MarshalHelper(overloadFilters);
        helper.marshal(marshalDir, getMD5(key), method, result);
        return result;
    }
    
    /**
     * replay object from marshal file
     * 
     * @param cls object class
     * @param method method name
     * @param inputs method inputs
     * @return method result object
     * @throws NoSuchAlgorithmException 
     */
    public Object replay(Class<?> cls, String method, Object... inputs) throws NoSuchAlgorithmException{
        String key = generateKey(method, inputs, true);
        try {
            logger.log(Level.INFO, "ServiceMock mode is REPLAY!, it replay record result!");
            if (isDebug(cls, method)) {
                logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            }
            MarshalHelper helper = new MarshalHelper(overloadFilters);
            File xmlFile = new File(marshalDir + "/" + getMD5(key) + ".txt");
            if (cls == null) cls = helper.getReturnObjectType(xmlFile);
            if (cls != null && debugCls == cls) logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            Object recordResult = helper.unmarshal(xmlFile, cls);
            return recordResult;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            throw new RuntimeException(e);
        }
    }
    
    /**
     * record array object to marshal file
     * 
     * @param cls array type class
     * @param result array result
     * @param method method name
     * @param inputs method inputs
     * @return method result array
     * @throws NoSuchAlgorithmException 
     */
    public Object[] recordArray(Object[] result, String method, Object... inputs) throws NoSuchAlgorithmException {
        String key = generateKey(method, inputs, true);
        try {
            if ((result != null && isDebug(result.getClass(), method))) {
                logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            }
            if (result == null){
                logger.log(Level.FINE, "result is null");
                return result;
            }
            MarshalHelper helper = new MarshalHelper(overloadFilters);
            helper.marshal(marshalDir, getMD5(key), method, result);
            return result;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    /**
     * replay array object fron marshal file
     * 
     * @param cls array type class
     * @param method method name
     * @param inputs input strings
     * @return result object
     * @throws NoSuchAlgorithmException 
     */
    public Object[] replayArray(Class<?> cls, String method, Object... inputs) throws NoSuchAlgorithmException {
        String key = generateKey(method, inputs, true);
        try {
            if (isDebug(cls, method)) {
                logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            }
            MarshalHelper helper = new MarshalHelper(overloadFilters);
            File xmlFile = new File(marshalDir + "/" + getMD5(key) + ".txt");
            if (cls == null) cls = helper.getReturnArrayType(xmlFile);
            if (cls != null && debugCls == cls.getComponentType()) logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            if (cls == null) return new Object[0];
            Object[] recordResult = helper.unmarshalArray(xmlFile, cls);
            return recordResult;
        } catch (Exception e){
            logger.log(Level.SEVERE, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            throw new RuntimeException(e);
        }
    }
    
    /**
     * record list object to marshal file
     * 
     * @param cls list component class
     * @param result list result
     * @param method method name
     * @param inputs method inputs
     * @return method result list
     */
    public List<?> recordList(List<?> result, String method, Object... inputs) {
        String key = generateKey(method, inputs, true);
        try {
            if ((result != null && isDebug(result.getClass(), method))) {
                logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            }
            if (result == null){
                logger.log(Level.FINE, "result is null");
                return result;
            }
            MarshalHelper helper = new MarshalHelper(overloadFilters);
            helper.marshal(marshalDir, getMD5(key), method, result);
            return result;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    /**
     * replay list object from marshal file
     * 
     * @param cls list type class
     * @param method method name
     * @param inputs inputs
     * @return list object
     * @throws NoSuchAlgorithmException 
     */
    @SuppressWarnings("rawtypes")
    public List<?> replayList(Class<?> cls, String method, Object... inputs) throws NoSuchAlgorithmException {
        String key = generateKey(method, inputs, true);
        try {
            if (isDebug(cls, method)) {
                logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            }
            MarshalHelper helper = new MarshalHelper(overloadFilters);
            File xmlFile = new File(marshalDir + "/" + getMD5(key) + ".txt");
            if (cls == null) cls = helper.getReturnListType(xmlFile);
            if (cls != null && cls == debugCls) logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            if (cls == null) return new ArrayList();
            List<?> recordResult = helper.unmarshalList(xmlFile, cls);
            return recordResult;
        } catch (Exception e){
            logger.log(Level.SEVERE, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            throw new RuntimeException(e);
        }
    }

    /**
     * record map object to marshal file
     * 
     * @param result map object
     * @param method method name
     * @param inputs input array
     * @return map object
     */
    public Map<?, ?> recordMap(Map<?, ?> result, String method, Object... inputs) {
        try {
            String key = generateKey(method, inputs, true);
            if ((result != null && result.size() > 0 && (isDebug(result.keySet().iterator().next().getClass(), method) || isDebug(result.values().iterator().next().getClass(), method)))) {
                logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            }
            if (result == null){
                logger.log(Level.FINE, "result is null");
                return result;
            }
            MarshalHelper helper = new MarshalHelper(overloadFilters);
            helper.marshal(marshalDir, getMD5(key), method, result);
            return result;
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
    
    /**
     * replay map object from marshal file
     * 
     * @param keyCls key class
     * @param valueCls value class
     * @param method method name
     * @param inputs inputs
     * @return map object
     * @throws NoSuchAlgorithmException 
     */
    @SuppressWarnings("rawtypes")
    public Map<?, ?> replayMap(Class<?> keyCls, Class<?> valueCls, String method, Object... inputs) throws NoSuchAlgorithmException {
        String key = generateKey(method, inputs, true);
        try {
            if (isDebug(keyCls, method) || isDebug(valueCls, method)) {
                logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            }
            logger.log(Level.INFO, "ServiceMock mode is REPLAY!, it replay record result!");
            MarshalHelper helper = new MarshalHelper(overloadFilters);
            File xmlFile = new File(marshalDir + "/" + getMD5(key) + ".txt");
            if (keyCls == null) keyCls = helper.getReturnMapKeyType(xmlFile);
            if (valueCls == null) valueCls = helper.getReturnMapValueType(xmlFile);
            if (keyCls != null && valueCls != null && (keyCls == debugCls || valueCls == debugCls)) logger.log(Level.INFO, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            if (keyCls == null || valueCls == null) return new HashMap();
            Map<?, ?> recordResult = helper.unmarshalMap(xmlFile, keyCls, valueCls);
            return recordResult;
        } catch (Exception e){
            logger.log(Level.SEVERE, "method:" + method + ", MD5:" + getMD5(key) + ", inputs:" + LINE_SEPARATOR + generateKey(method, inputs, false));
            throw new RuntimeException(e);
        }
    }
    
    /**
     * generate method key for recording
     * 
     * @param method method name
     * @param inputs method inputs, it should be string, if not please define its unique string key 
     * @return method key
     */
    private static String generateKey(String method, Object[] inputs, boolean includeMethod) {
        StringBuilder bud = new StringBuilder();
        if (includeMethod) bud.append(method + "-");
        if (inputs == null || inputs.length == 0){
            bud.append("void");
            return bud.toString();
        } else {
        	ValueMapTool tool = new ValueMapTool();
        	for (int i = 0; i < inputs.length; i++){
                bud.append(tool.getObjectValue("input" + i, inputs[i]) + ValueMapTool.LINE_SEPARATOR);
            }
        }
        return bud.toString();
    }
    
    /**
     * get MD5 hash string
     * 
     * @param input input string
     * @return hash string
     * @throws NoSuchAlgorithmException
     */
    public static String getMD5(String input) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(input.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);
        // Now we need to zero pad it if you actually want the full 32 chars.
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }
        return hashtext;
    }
    
    /**
     * write text to file
     * 
     * @param inputText text to file
     * @param file output file
     * @throws IOException
     */
    public static void writeTextToFile(String inputText, File file) throws IOException {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(inputText + "\n");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }
    
    public boolean isDebug(Class<?> cls, String method){
        if (cls != null && cls == debugCls) {
            return true;
        }
        if (method != null && method.equalsIgnoreCase(debugMethod)) {
            return true;
        }
        return false;
    }
     
}