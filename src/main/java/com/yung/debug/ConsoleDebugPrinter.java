package com.yung.debug;

public class ConsoleDebugPrinter extends AbstractDebugPrinter {

    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }

}