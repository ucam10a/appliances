package com.yung.debug;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.TreeSet;

/**
 * Construct a object string value
 * It also normalizes to object values
 * For example, HashSet<String> elements will be printed by sorted order
 * 
 * @author Yung Long Li
 *
 */
public class ValueMapTool extends AbstractDebugPrinter {

    public ValueMapTool(){
        super();
        this.showFieldType = true;
        this.showNull = false;
    }
    
	private class Triple implements Comparable<Triple> {
		int hashcode;
		Object obj1;
		Object obj2;
		public Triple(int hashcode, Object obj1, Object obj2) {
			this.hashcode = hashcode;
			this.obj1 = obj1;
			this.obj2 = obj2;
		}
		public int compareTo(Triple triple) {
			if (this.hashcode > triple.hashcode) {
				return 1; 
			} else if (this.hashcode < triple.hashcode) {
				return -1;
			} else {
				if (this.obj1.hashCode() == triple.obj1.hashCode()) {
					return 0;
				} else if (this.obj1.hashCode() > triple.obj1.hashCode()) {
					return 1;
				} else {
					return -1;
				}
			}
		}
	}
	
	@Override
	public void printMessage(String message) {
		// ignore
	}
	
	@Override
	protected RecordBuffer printSet(RecordBuffer bud, String indent, String objName, Set<?> set) {
        try {
            int i = 1;
            bud = printMarkMessage(bud, indent, set.getClass().getName() + ": " + objName, set.getClass().getName().hashCode());
            TreeSet<Triple> temp = new TreeSet<Triple>();
            for (Object obj : set) {
            	temp.add(new Triple(getNormalizedHashcode(obj), obj, null));
            }
            for (Triple triple : temp) {
                bud = printObjectParam(bud, indent + "    ", objName + i, triple.obj1);
                i++;
            }
            return bud;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	@Override
	protected RecordBuffer printMap(RecordBuffer bud, String indent, String objName, Map<?, ?> map) {
        try {
            int i = 1;
            bud = printMarkMessage(bud, indent, map.getClass().getName() + ": " + objName, map.getClass().getName().hashCode());
            TreeSet<Triple> temp = new TreeSet<Triple>();
            for (Entry<?, ?> entry : map.entrySet()) {
            	temp.add(new Triple(getNormalizedHashcode(entry.getKey()), entry.getKey(), entry.getValue()));
            }
            for (Triple triple : temp) {
                bud = printObjectParam(bud, indent + "    ", "key" + i, triple.obj1);
                bud = printObjectParam(bud, indent + "    ", "value" + i, triple.obj2);
                i++;
            }
            return bud;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	public String getObjectValue(String name, Object input) {
		return getObjectValue(name, input, false);
	}
	
	public String getObjectValue(String name, Object input, boolean debug) {
        RecordBuffer bud = new RecordBuffer();
        bud = printObjectParam(bud, "", name, input);
        return bud.toString();
    }
	
	
}