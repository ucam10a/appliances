package com.yung.jpa;

interface Visitor<T extends Visitable<?>>
{
    public void visit(T item);
    
    public boolean wasVisited(T item);
    
}