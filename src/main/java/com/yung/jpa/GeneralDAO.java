package com.yung.jpa;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

public class GeneralDAO {

    EntityManager mgr;
    
    public GeneralDAO(DataSource ds){
        mgr = new EntityManager(ds, null);
    }
    
    public GeneralDAO(Connection conn){
        mgr = new EntityManager(null, conn);
    }
    
    public <T> List<T> find(Class<T> cls, String sql, Object... params) throws Exception {
        return mgr.findBySql(cls, sql, params);
    }
    
    public <T> T findOne(Class<T> cls, String sql, Object... params) throws Exception {
        List<T> result = this.find(cls, sql, params);
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
    
    public List<Map<String, Object>> find(String sql, Object... params) throws Exception {
        return mgr.findBySql(sql, params);
    }
    
    public Map<String, Object> findOne(String sql, Object... params) throws Exception {
        List<Map<String, Object>> result = this.find(sql, params);
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }
    
}