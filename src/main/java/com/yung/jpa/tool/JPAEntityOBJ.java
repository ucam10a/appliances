package com.yung.jpa.tool;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.yung.jpa.tool.Attribute.Type;

public class JPAEntityOBJ extends EntityOBJ {

    private String schema;
    
    public JPAEntityOBJ(){
    }
    
    public JPAEntityOBJ(String schema, String className, String tableName, List<Attribute> attributes) {
        this.schema = schema;
        this.className = className;
        this.tableName = tableName;
        this.attributes = attributes;
    }

    public static List<Attribute> getAttributes(Connection conn, String schema, String table) throws SQLException {
        List<Attribute> columns = new ArrayList<Attribute>();
        DatabaseMetaData dbmd = conn.getMetaData();
        ResultSet rs = dbmd.getPrimaryKeys(null, null, table.toUpperCase());
        Set<String> keyColumns = new HashSet<String>();
        while(rs.next()){
            keyColumns.add(rs.getString("COLUMN_NAME").toUpperCase());
        }
        Statement st = conn.createStatement();
        String sql = null;
        if (schema != null && !schema.equals("")) {
            sql = "SELECT * FROM " + schema + "." + table;
        } else {
            sql = "SELECT * FROM " + table;
        }
        rs = st.executeQuery(sql);
        ResultSetMetaData rsMetaData = rs.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();
        for (int i = 1; i <= numberOfColumns; i++) {
            rsMetaData.getColumnTypeName(i);
            String columnName = rsMetaData.getColumnName(i);
            String type = rsMetaData.getColumnTypeName(i);
            boolean isKey = false;
            if (keyColumns.contains(columnName.toUpperCase())) {
                isKey = true;
            }
            columns.add(new Attribute(getType(type), rename(columnName), columnName, isKey));
        }
        return columns;
    }

    private static Type getType(String type) {
        // oracle
        if (type.toLowerCase().contains("string")) {
            return Type.String;
        } else if (type.toLowerCase().contains("integer")) {
            return Type.Integer;
        } else if (type.toLowerCase().contains("int")) {
            return Type.Integer;
        } else if (type.toLowerCase().contains("timestamp")) {
            return Type.Timestamp;
        } else if (type.toLowerCase().contains("boolean")) {
            return Type.Boolean;
        } else if (type.toLowerCase().contains("date")) {
            return Type.Timestamp;
        } else if (type.toLowerCase().contains("long")) {
            return Type.Long;
        } else if (type.toLowerCase().contains("double")) {
            return Type.Double;
        } else if (type.toLowerCase().contains("biginteger")) {
            return Type.BigInteger;
        } else if (type.toLowerCase().contains("bigdecimal")) {
            return Type.BigDecimal;
        } else if (type.toLowerCase().contains("varchar2")) {
            return Type.String;
        } else if (type.toLowerCase().contains("clob")) {
            return Type.Clob;
        } else if (type.toLowerCase().contains("number")) {
            return Type.BigDecimal;
        } else if (type.toLowerCase().contains("varchar")) {
            return Type.String;
        } else {
            throw new RuntimeException("unknown type: " + type);
        }
    }

    private static String rename(String columnName) {
        if (columnName.contains("_")) {
            String[] tokens = columnName.split("_");
            StringBuilder bud = new StringBuilder();
            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i].trim().toLowerCase();
                if (i != 0) {
                    bud.append(upperFirstChar(token));
                } else {
                    bud.append(token);
                }
            }
            return bud.toString();
        } else {
            return columnName.toLowerCase();
        }
    }
    
    public String generateCode(){
        bud = new StringBuilder();
        // Entity
        append("@Entity");
        append("@Table(name=\"" + tableName + "\", schema=\"" + schema + "\")");
        append("@IdClass(" + upperFirstChar(className) + "Id.class)");
        append("public class " + upperFirstChar(className) + " implements Serializable {");
        for (Attribute attr : attributes) {
            append("");
            if (attr.isKey()) {
                append("    @Id");
            }
            append("    @Column(name=\"" + attr.getAttributeName() + "\")");
            append("    private " + attr.getType() + " " + attr.getObjectName() + ";");
        }
        for (Attribute attr : attributes) {
            append("");
            append("    public void set" + upperFirstChar(attr.getObjectName()) + "(" + attr.getType() + " " + attr.getObjectName() + ") {");
            append("        this." + attr.getObjectName() + " = " + attr.getObjectName() + ";");
            append("    }");
            append("");
            append("    public " + attr.getType() + " get" + upperFirstChar(attr.getObjectName()) + "() {");
            append("        return " + attr.getObjectName() + ";");
            append("    }");
        }
        append("");
        append("    public " + upperFirstChar(className) + "Id getId() {");
        append("        " + upperFirstChar(className) + "Id id = new " + upperFirstChar(className) + "Id();");
        for (Attribute attr : attributes) {
            if (attr.isKey()) {
                append("        id.set" + upperFirstChar(attr.getObjectName()) + "(this." + attr.getObjectName() + ");");
            }
        }
        append("        return id;");
        append("    }");
        append("}");
        append("\n\n");
        append("public class " + upperFirstChar(className) + "Id implements Serializable {");
        for (Attribute attr : attributes) {
            if (attr.isKey()) {
                append("");
                append("    private " + attr.getType() + " " + attr.getObjectName() + ";");
            }
        }
        for (Attribute attr : attributes) {
            if (attr.isKey()) {
                append("");
                append("    public void set" + upperFirstChar(attr.getObjectName()) + "(" + attr.getType() + " " + attr.getObjectName() + ") {");
                append("        this." + attr.getObjectName() + " = " + attr.getObjectName() + ";");
                append("    }");
                append("");
                append("    public " + attr.getType() + " get" + upperFirstChar(attr.getObjectName()) + "() {");
                append("        return " + attr.getObjectName() + ";");
                append("    }");
            }
        }
        append("}");
        return bud.toString();
    }
    
    public static void main(String[] args) throws Exception {
        
        
        
    }
}