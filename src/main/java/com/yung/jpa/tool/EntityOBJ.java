package com.yung.jpa.tool;

import java.util.ArrayList;
import java.util.List;

import com.yung.jpa.tool.Attribute.Type;

public class EntityOBJ {

    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    protected String className;
    
    protected String tableName;
    
    protected List<Attribute> attributes = new ArrayList<Attribute>();
    
    protected StringBuilder bud = new StringBuilder();
    
    public EntityOBJ(){
    }
    
    public EntityOBJ(String className, String tableName, Attribute... attributes){
        this.className = className;
        this.tableName = tableName;
        for (Attribute attr : attributes) {
            this.attributes.add(attr);
        }
    }
    
    public EntityOBJ(String className, String tableName, List<Attribute> attributes){
        this.className = className;
        this.tableName = tableName;
        this.attributes = attributes;
    }
    
    public void append(String code) {
        bud.append(code + LINE_SEPARATOR);
    }
    
    public String generateCode(){
        bud = new StringBuilder();
        // Entity
        append("@Entity(name=\"" + tableName + "\")");
        append("@EntityListeners(value={" + className + "DAO.class})");
        append("public class " + className + " extends BasicEntity implements Serializable {");
        for (Attribute attr : attributes) {
            append("");
            append("    @Column(name=\"" + attr.getAttributeName() + "\")");
            if (attr.isKey()) {
                append("    @Id");
            }
            append("    private " + attr.getType() + " " + attr.getObjectName() + ";");
        }
        for (Attribute attr : attributes) {
            append("");
            append("    public void set" + upperFirstChar(attr.getObjectName()) + "(" + attr.getType() + " " + attr.getObjectName() + ") {");
            append("        this." + attr.getObjectName() + " = " + attr.getObjectName() + ";");
            append("    }");
            append("");
            append("    public " + attr.getType() + " get" + upperFirstChar(attr.getObjectName()) + "() {");
            append("        return " + attr.getObjectName() + ";");
            append("    }");
        }
        append("}");
        // DAO
        append("");
        append("public class " + className + "DAO extends AbstractDAO<" + className + "> {");
        append("}");
        return bud.toString();
    }

    protected static String upperFirstChar(String objectName) {
        String name = objectName.trim();
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            String character = name.charAt(i) + "";
            if (i == 0) {
                character = character.toUpperCase();
            }
            buf.append(character);
        }
        return buf.toString();
    }
    
    public static void main(String[] args) throws Exception {
        
        EntityOBJ entity = new EntityOBJ(
                "UserOBJ", "USERS"
                , new Attribute(Type.String, "id", "ID", true)
                , new Attribute(Type.String, "name", "NAME", false)
                , new Attribute(Type.Timestamp, "createDate", "CREATE_DT", false)
                , new Attribute(Type.String, "createUser", "CREATE_USER", false)
                );
        
        String code = entity.generateCode();
        System.out.println(code);
        
        
    }

}