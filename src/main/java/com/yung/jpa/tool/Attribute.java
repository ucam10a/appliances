package com.yung.jpa.tool;

public class Attribute {

    public enum Type {
        String, Integer, Timestamp, Boolean, Date, Long, Double, BigInteger, BigDecimal, Clob
    }
    
    public Type getType(String type){
        return Type.String;
    }
    
    private Type type;
    
    private String objectName;
    
    private String attributeName;
    
    private boolean isKey = false;
    
    public Attribute(){
    }
    
    public Attribute(Type type, String objectName, String attributeName, boolean isKey) {
        this.type = type;
        this.objectName = objectName;
        this.attributeName = attributeName;
        this.isKey = isKey;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public boolean isKey() {
        return isKey;
    }

    public void setKey(boolean isKey) {
        this.isKey = isKey;
    }
    
}