package com.yung.jpa;

import java.util.List;

public interface IntefaceDAO<T> {

    public void insert(T object) throws Exception;
    
    public void insert(List<T> object) throws Exception;
    
    public void update(T object) throws Exception;
    
    public void update(T object, String... fields) throws Exception;
    
    public void update(List<T> object) throws Exception;
    
    public void delete(T object) throws Exception;
    
    public void delete(List<T> object) throws Exception;
    
    public List<T> find(T object) throws Exception;
    
    public List<T> find(String whereSql, Object... params) throws Exception;
    
    public T findOne(String whereSql, Object... params) throws Exception;
    
    public void deleteBySql(String whereSql, Object... params) throws Exception;
    
    public T findByPrimaryKey(T object) throws Exception;
    
    public List<T> listAll() throws Exception;
    
    public void setKeyMapping(T object) throws Exception;
    
    public void setEntityManager(EntityManager mgr);
    
    public void flush();

    public void setKeyMapping(List<T> objects) throws Exception;
    
}