package com.yung.jpa;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;

public class NumberConverter extends ObjectConverter {

    public boolean isNumber(Type type) {
        Class<?> cls = getClass(type);
        return isNumber(cls);
    }
    
    public boolean isNumber(Class<?> cls) {
        if (cls == Integer.class) {
            return true;
        } else if (cls == Float.class) {
            return true;
        } else if (cls == Double.class) {
            return true;
        } else if (cls == Long.class) {
            return true;
        } else if (cls == BigInteger.class) {
            return true;
        } else if (cls == BigDecimal.class) {
            return true;
        } 
        return false;
    }
    
    public Object Convert(Object obj,  Class<?> cls) {
        BigDecimal decimal = new BigDecimal(0);
        if (obj.getClass() == Integer.class) {
            decimal = new BigDecimal((Integer) obj);
        } else if (obj.getClass() == Float.class) {
            decimal = new BigDecimal((Float) obj);
        } else if (obj.getClass() == Double.class) {
            decimal = new BigDecimal((Double) obj);
        } else if (obj.getClass() == Long.class) {
            decimal = new BigDecimal((Long) obj);
        } else if (obj.getClass() == BigInteger.class) {
            decimal = new BigDecimal((BigInteger) obj);
        } else if (obj.getClass() == BigDecimal.class) {
            decimal = (BigDecimal) obj;
        } else if (obj.getClass() == String.class && isNumeric(obj.toString())) {
            decimal = new BigDecimal(obj.toString());
        } else {
            throw new RuntimeException("unsupport obj: " + obj.getClass().getName());
        }
        if (cls == Integer.class) {
            return decimal.intValue();
        } else if (cls == Float.class) {
            return decimal.floatValue();
        } else if (cls == Double.class) {
            return decimal.doubleValue();
        } else if (cls == Long.class) {
            return decimal.longValue();
        } else if (cls == BigInteger.class) {
            return decimal.toBigInteger();
        } else if (cls == BigDecimal.class) {
            return decimal;
        } else {
            throw new RuntimeException("unsupport obj: " + obj.getClass().getName());
        }
    }
    
    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
    
}
