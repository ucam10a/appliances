package com.yung.jpa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialClob;
import javax.sql.rowset.serial.SerialException;

public class ClobConverter extends ObjectConverter {
    
    @Override
    public Object Convert(Object obj, Class<?> cls) throws Exception {
        if (cls != String.class && cls != Clob.class) {
            throw new RuntimeException("unsupport class: " + cls.getName());
        }
        if (obj instanceof Clob && cls == String.class) {
            Clob clob = (Clob) obj;
            return ClobToString(clob);
        } else if (obj instanceof String &&  cls == Clob.class) {
            String str = (String) obj;
            return StringToClob(str);
        } else {
            throw new RuntimeException("unsupport obj: " + obj.getClass().getName() + ", class: " + cls.getName());
        }
    }
    
    private static Clob StringToClob(String input) throws SerialException, SQLException{
        Clob clob = new SerialClob(input.toCharArray());
        return clob;
    }
 
    private static String ClobToString(Clob clob) throws SQLException, IOException {
        final StringBuilder sb = new StringBuilder();
        final Reader reader = clob.getCharacterStream();
        final BufferedReader br = new BufferedReader(reader);
        int b;
        while (-1 != (b = br.read())) {
            sb.append((char) b);
        }
        br.close();
        return sb.toString();
    }

}