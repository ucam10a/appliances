package com.yung.jpa;

import java.util.HashMap;
import java.util.Map;

public abstract class BasicEntity {

    private Map<String, Object> cacheId = new HashMap<String, Object>();
    
    public boolean isEmptyCacheId(){
        if (cacheId.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public void setId(String pkName, Object pkValue){
        cacheId.put(pkName, pkValue);
    }
    
    public Object getId(String pkName){
        return cacheId.get(pkName);
    }
    
}