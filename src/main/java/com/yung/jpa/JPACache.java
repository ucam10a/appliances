package com.yung.jpa;

import java.util.List;

public class JPACache {

    public static boolean isCache = false;
    
    public static void cache(Object printer, String packagePath){
        List<Class<?>> classes = ClassFinder.find(packagePath);
        for (Class<?> cls : classes){
            BasicEntityManager.setEntityClassMap(cls.getName());
        }
        BasicEntityManager.setAllFieldClass(printer);
        isCache = true;
    }
    
}