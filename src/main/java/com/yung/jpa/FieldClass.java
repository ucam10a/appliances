package com.yung.jpa;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.persistence.*;

class FieldClass {
    
    Entity e;
    
    Column c;
    
    Id id;
    
    GeneratedValue g;
    
    JoinColumns _1to1_cols;
    
    OneToOne _1to1;
    
    JoinColumns _1toM_cols;
    
    OneToMany _1toM;
    
    Field f;
    
    Method getter;
    
    Method setter;

}
