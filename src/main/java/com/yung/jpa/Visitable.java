package com.yung.jpa;

interface Visitable<V extends Visitor<?>>
{
    public void accept(V visitor);
}