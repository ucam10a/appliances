package com.yung.jpa;

import java.lang.reflect.Type;

public abstract class ObjectConverter {

    public static Class<?> getClass(Type type) {
        try {
            String typeStr = type.toString();
            if (typeStr.equalsIgnoreCase("int")) {
                return Integer.class;
            } else if (typeStr.equalsIgnoreCase("double")) {
                return Double.class;
            } else if (typeStr.equalsIgnoreCase("float")) {
                return Float.class;
            } else if (typeStr.equalsIgnoreCase("long")) {
                return Long.class;
            } else if (typeStr.equalsIgnoreCase("boolean")) {
                return Boolean.class;
            } else {
                int index = typeStr.indexOf("class");
                if (index != -1) {
                    typeStr = typeStr.substring("class".length()).trim();
                }
                index = typeStr.indexOf("interface");
                if (index != -1) {
                    typeStr = typeStr.substring("interface".length()).trim();
                }
                return Class.forName(typeStr);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public Object Convert(Object obj, Type type) throws Exception {
        Class<?> cls = getClass(type);
        return Convert(obj, cls);
    }
    
    public abstract Object Convert(Object obj,  Class<?> cls) throws Exception;
}
