package com.yung.jpa;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.persistence.*;
import javax.sql.DataSource;

public class BasicEntityManager extends PlainObjectOperator {
    
    private static Boolean debug = false;
    
    private static Map<String, FieldClass[]> fieldMap = new HashMap<String, FieldClass[]>();
    
    private static Map<String, EntityClass> tableMap = new HashMap<String, EntityClass>();
    
    private static NumberConverter nConverter = new NumberConverter();
    private static TimeConverter tConverter = new TimeConverter();
    
    protected Connection connection;
    
    private DataSource dataSource;
    
    public static EntityClass getEntityClass(String tableName){
        return tableMap.get(tableName);
    }
    
    public static void setEntityClassMap(String className){
        Class<?> cls;
        try {
            cls = Class.forName(className);
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
            throw new RuntimeException(e2);
        }
        Entity e = cls.getAnnotation(Entity.class);
        if (e == null) return;
        if (!BasicEntity.class.isAssignableFrom(cls)) {
            return;
        }
        EntityListeners listener = cls.getAnnotation(EntityListeners.class);
        EntityClass ec = new EntityClass();
        if (getEntityClass(e.name()) == null){
            try {
                Object dao = listener.value()[0].newInstance();
                AbstractDAO<?> adao = null;
                if (dao instanceof AbstractDAO){
                    adao = (AbstractDAO<?>) dao;
                }
                ec.setTable(e.name());
                ec.setDaoClassName(listener.value()[0].getName());
                if (adao == null){
                    throw new Exception(listener.value()[0].getName() + "does not extends AbstractDAO");
                }
                ec.setClassName(adao.getType().getName());
                tableMap.put(e.name(), ec);
            } catch (Exception e1){
                e1.printStackTrace();
                throw new RuntimeException(e1);
            }
        } else {
            if (!getEntityClass(e.name()).getClassName().equals(className)) {
                throw new RuntimeException("className: " + getEntityClass(e.name()).getClassName() + " already defined table: " + e.name() + "! Please check annotation of " + className);
            }
        }
    }
    
    public static void setAllFieldClass(Object printTool){
        DebugPrinterTool printer = null;
        if (debug && printTool != null) {
            printer = new DebugPrinterTool(printTool);
            BasicEntityManager.debugTable(printer);
        }
        for (EntityClass ec : tableMap.values()){
            if (ec.getClassName() == null){
                if (printer != null) printer.printObjectParam(ec);
                throw new RuntimeException("ec.className is null");
            }
            getFieldClass(ec.getClassName());
        }
    }
    
    public static FieldClass[] getFieldClass(String className) {
        FieldClass[] result = fieldMap.get(className);
        if (result != null) {
            return result;
        } else {
            Class<?> objClass = null;
            try {
                objClass = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            int counter = 0;
            Class<?> cls = objClass;
            while (cls != null) {
                Field[] fArr = cls.getDeclaredFields();
                for (Field f : fArr){
                    Column c = f.getAnnotation(Column.class);
                    GeneratedValue g = f.getAnnotation(GeneratedValue.class);
                    Id id = f.getAnnotation(Id.class);
                    JoinColumns cols = f.getAnnotation(JoinColumns.class);
                    JoinTable jtb = f.getAnnotation(JoinTable.class);
                    OneToOne _1to1 = f.getAnnotation(OneToOne.class);
                    OneToMany _1toM = f.getAnnotation(OneToMany.class);
                    if (c != null || g != null || id != null || cols != null || jtb != null || _1to1 != null || _1toM != null) {
                        counter++;
                    }
                }
                cls = cls.getSuperclass();
            }
            Entity e = objClass.getAnnotation(Entity.class);
            if (e == null) throw new RuntimeException(className + " doesn't have Entity annotation!");
            result = new FieldClass[counter];
            cls = objClass;
            int idx = 0;
            List<String> columns = new ArrayList<String>();
            List<String> pkColumns = new ArrayList<String>();
            while (cls != null) {
                Field[] fArr = cls.getDeclaredFields();
                for (Field f : fArr){
                    Column c = f.getAnnotation(Column.class);
                    GeneratedValue g = f.getAnnotation(GeneratedValue.class);
                    Id id = f.getAnnotation(Id.class);
                    JoinColumns cols = f.getAnnotation(JoinColumns.class);
                    JoinTable jtb = f.getAnnotation(JoinTable.class);
                    OneToOne _1to1 = f.getAnnotation(OneToOne.class);
                    OneToMany _1toM = f.getAnnotation(OneToMany.class);
                    if (c != null || g != null || id != null || cols != null || jtb != null || _1to1 != null || _1toM != null) {
                        if (f.getType().getName().equals("boolean") || f.getType().getName().equals("int") || f.getType().getName().equals("long") || f.getType().getName().equals("float") || f.getType().getName().equals("double")){
                            throw new RuntimeException(className + " " + f.getName() + " is not allowed to use primitive type");
                        }
                        FieldClass fc = new FieldClass();
                        fc.e = e;
                        fc.c = c;
                        fc.g = g;
                        fc.id = id;
                        fc._1to1 = _1to1;
                        if (_1to1 != null) fc._1to1_cols = cols;
                        fc._1toM = _1toM;
                        if (_1toM != null) fc._1toM_cols = cols;
                        String methodName = "set" + f.getName();
                        for (Method method : cls.getMethods()) {
                            if ((method.getName().toLowerCase().equals(methodName.toLowerCase()) && method.getParameterTypes().length == 1)) {
                                fc.setter = method;
                                break;
                            }
                        }
                        if (fc.setter == null){
                            throw new RuntimeException("There is no such " + methodName + " method for " + className);
                        }
                        methodName = "get" + f.getName();
                        for (Method method : cls.getMethods()) {
                            if ((method.getName().toLowerCase().equals(methodName.toLowerCase()) && method.getParameterTypes().length == 0)) {
                                fc.getter = method;
                                break;
                            }
                        }
                        if (fc.getter == null){
                            throw new RuntimeException("There is no such " + methodName + " method for " + className);
                        }
                        if (c != null){
                            if (!columns.contains(c.name())) columns.add(c.name());
                            if (!pkColumns.contains(c.name())) if (id != null) pkColumns.add(c.name());
                        }
                        fc.f = f;
                        result[idx] = fc;
                        idx++;
                        if (c != null) {
                            tableMap.get(e.name()).setColumnFieldClass(c.name(), fc);
                        }
                    }
                }
                cls = cls.getSuperclass();
            }
            String[] cols = ToArray(String.class, columns);
            String[] pkCols = ToArray(String.class, pkColumns);
            tableMap.get(e.name()).setColumns(cols);
            tableMap.get(e.name()).setPkColumns(pkCols);
        }
        fieldMap.put(className, result);
        ClassVisitor visitor = new ClassVisitor();
        checkReference(className, visitor);
        return result;
    }
    
    public static void checkReference(String className, ClassVisitor visitor) {
        VisitedClass cls = new VisitedClass(className);
        if (visitor.wasVisited(cls)){
            throw new RuntimeException("There is an infinite loop [" + visitor.toString() + "=>" + cls.toString() + "]");
        }
        cls.accept(visitor);
        for (FieldClass f : getFieldClass(className)){
            String _className = null;
            if (f._1to1 != null){
                _className = getEntityClass(f._1to1_cols.value()[0].table()).getClassName();
            }
            if (f._1toM != null){
                _className = getEntityClass(f._1toM_cols.value()[0].table()).getClassName();
            }
            if (_className != null){
                ClassVisitor newVisitor = visitor.clone();
                checkReference(_className, newVisitor);
            }
        }
    }
    
    public static String getTableName(Object obj){
        BasicEntityManager.checkEntityAnnotation(obj.getClass());
        FieldClass[] fields = getFieldClass(obj.getClass().getName());
        return fields[0].e.name();
    }
    
    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
    public static void checkEntityAnnotation(Class<?> objClass){
        Entity entity = objClass.getAnnotation(Entity.class);
        if (entity == null){
            throw new RuntimeException("The " + objClass.getName() + " doesn't have Entity annotation!");
        }
        if (tableMap.get(entity.name()) == null) {
            setEntityClassMap(objClass.getName());
            getFieldClass(objClass.getName());
        }
    }
    
    public String createInsertSql(Class<?> objClass) throws ClassNotFoundException {
        String table = getTableName(objClass);
        EntityClass ec = EntityManager.getEntityClass(table);
        StringBuilder sql = new StringBuilder("INSERT INTO ");
        sql.append(table + " (");
        String sqlColumns = concatenateCol(ec.getColumns());
        String sqlParameterSymbols = createSymbols(ec.getColumns());
        sql.append(sqlColumns);
        sql.append(") VALUES (");
        sql.append(sqlParameterSymbols);
        sql.append(")");
        return sql.toString();
    }
    
    public String concatenateCol(String[] columns) {
        StringBuilder result = new StringBuilder();
        for (String col : columns){
            result.append(col + ", ");
        }
        String sql = result.toString();
        if (sql == null || sql.equals("")) throw new RuntimeException("There is no Column annotation in class!");
        return sql.substring(0, sql.length() - 2);
    }

    public String createSymbols(String[] columns) {
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < columns.length; i++) result.append("?, ");
        String str = result.toString();
        if (str == null || str.equals("")) throw new RuntimeException("There is no Column annotation in class!");
        return str.substring(0, str.length() - 2);
    }

    public String createUpdateSql(Class<?> objClass) throws ClassNotFoundException {
        String table = getTableName(objClass);
        EntityClass ec = getEntityClass(table);
        StringBuilder result = new StringBuilder();
        result.append("UPDATE ");
        result.append(table);
        result.append(" SET ");
        result.append(createSetSyntax(ec.getColumns()));
        result.append(" WHERE ");
        result.append(createWherePKSyntax(ec.getPkColumns()));
        return result.toString();
    }
    
    public String createUpdateSql(Class<?> objClass, String... fields) throws ClassNotFoundException {
        String table = getTableName(objClass);
        EntityClass ec = getEntityClass(table);
        StringBuilder result = new StringBuilder();
        result.append("UPDATE ");
        result.append(table);
        result.append(" SET ");
        String[] compileFields = new String[fields.length];
        for (FieldClass f : getFieldClass(objClass.getName())){
            String name = f.f.getName();
            if (f.c != null) {
                String columnName = f.c.name();
                for (int i = 0; i < fields.length; i++) {
                    if (name.equals(fields[i])) {
                        compileFields[i] = columnName;
                    }
                }
            }
        }
        for (int i = 0; i < fields.length; i++) {
            if (compileFields[i] == null || compileFields[i].trim().equals("")) {
                compileFields[i] = fields[i];
            }
        }
        result.append(createSetSyntax(compileFields));
        result.append(" WHERE ");
        result.append(createWherePKSyntax(ec.getPkColumns()));
        return result.toString();
    }
    
    public String createDeleteSql(Class<?> objClass) throws ClassNotFoundException {
        String table = getTableName(objClass);
        EntityClass ec = getEntityClass(table);
        StringBuilder result = new StringBuilder();
        result.append("DELETE FROM ");
        result.append(table);
        result.append(" WHERE ");
        result.append(createWherePKSyntax(ec.getPkColumns()));
        return result.toString();
    }

    public String createWherePKSyntax(String[] PKColumns) throws ClassNotFoundException {
        StringBuilder temp = new StringBuilder();
        for(String name : PKColumns) temp.append(name + " = ? AND ");
        String result = temp.toString();
        if (result == null || result.equals("")) throw new RuntimeException("There is no Column annotation in class or not define primary key!");
        return result.substring(0, result.length() - 4);
    }
    
    public String createWhereAllSyntax(String[] PKColumns) {
        StringBuilder temp = new StringBuilder();
        for (String key : PKColumns){
            temp.append(key + " LIKE '%' ");
            break;
        }
        String result = temp.toString();
        if (result == null || result.equals("")) throw new RuntimeException("There is no Column annotation in class or not define primary key!");
        return result;
    }

    public String createSetSyntax(String[] columns) throws ClassNotFoundException {
        StringBuilder temp = new StringBuilder();
        for (String col : columns) temp.append(col + " = ?, ");
        String result = temp.toString();
        if (result == null || result.equals("")) throw new RuntimeException("There is no Column annotation in class or not define primary key!");
        return result.substring(0, result.length() - 2);
    }

    public String getTableName(Class<?> objClass) throws ClassNotFoundException {
        Entity e = objClass.getAnnotation(Entity.class);
        String result = "";
        if (e != null){
            result = result + e.name();
        }
        if (result == null || result.equals("")) throw new RuntimeException("There is no Table annotation in class or not define table name!");
        return result;
    }

    public int setParameters(PreparedStatement ps, Object obj) throws Exception {
        Entity en = obj.getClass().getAnnotation(Entity.class);
        EntityClass ec = EntityManager.getEntityClass(en.name());
        int counter = 1;
        for (String col : ec.getColumns()){
            setParam(ps, counter, ec.getColumnFieldClass(col).getter.invoke(obj));
            counter++;
        }
        return counter;
    }
    
    public int setParameters(PreparedStatement ps, Object obj, String... fields) throws Exception {
        Entity en = obj.getClass().getAnnotation(Entity.class);
        EntityClass ec = EntityManager.getEntityClass(en.name());
        int counter = 1;
        String[] tableColumnNames = new String[fields.length];
        for (FieldClass f : getFieldClass(obj.getClass().getName())){
            String name = f.f.getName();
            if (f.c != null) {
                String columnName = f.c.name();
                for (int i = 0; i < fields.length; i++) {
                    if (name.equals(fields[i])) {
                        tableColumnNames[i] = columnName;
                    }
                }
            }
        }
        for (int i = 0; i < fields.length; i++) {
            if (tableColumnNames[i] == null || tableColumnNames[i].trim().equals("")) {
                tableColumnNames[i] = fields[i];
            }
        }
        for (String col : tableColumnNames){
            setParam(ps, counter, ec.getColumnFieldClass(col).getter.invoke(obj));
            counter++;
        }
        return counter;
    }
    
    public void setParameters(PreparedStatement ps, Object obj, EntityClass ec, List<String> tables) throws Exception {
        int idx = 1;
        for (FieldClass f : getFieldClass(obj.getClass().getName())){
            Object value = invokeGetter(f, obj);
            if (value != null && f.c != null){
                setParam(ps, idx, value);
                idx++;
            }
            if (value != null && f._1to1 != null){
                for (int i = 0; i < f._1to1_cols.value().length; i++){
                    idx = setParameters(ps, value, idx);
                }
            }
        }
    }
    
    private int setParameters(PreparedStatement ps, Object obj, int idx) throws Exception {
        for (FieldClass f : getFieldClass(obj.getClass().getName())){
            Object value = invokeGetter(f, obj);
            if (value != null && f.c != null){
                setParam(ps, idx, value);
                idx++;
            }
            if (value != null && f._1to1 != null){
                for (int i = 0; i < f._1to1_cols.value().length; i++){
                    idx = setParameters(ps, value, idx);
                }
            }
        }
        return idx;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] ToArray(Class<T> cls, List<T> input) {
        final T[] result = (T[]) Array.newInstance(cls, input.size());
        for (int i = 0; i < input.size(); i++) result[i] = input.get(i);
        return result;
    }
    
    public int getColumnNum(PreparedStatement ps, Object obj) throws Exception {
        Field[] fields = obj.getClass().getDeclaredFields();
        int counter = 0;
        for (Field f : fields){
            Column c = f.getAnnotation(Column.class);
            if (c != null){
                counter++;
            }
        }
        if (counter == 0) throw new RuntimeException("There is no Column annotation in class!");
        return counter;
    }
    
    public <T extends BasicEntity> void setPrimaryKeyParameters(PreparedStatement ps, T obj) throws Exception {
        int counter = 1;
        setPrimaryKeyParameters(counter, ps, obj);
    }
    
    public <T extends BasicEntity> void setPrimaryKeyParameters(int index, PreparedStatement ps, T obj) throws Exception {
        Entity en = obj.getClass().getAnnotation(Entity.class);
        String table = en.name();
        EntityClass ec = EntityManager.getEntityClass(table);
        for (String col : ec.getPkColumns()){
            Object value = obj.getId(col);
            if (value == null) throw new Exception("primary key " + col + " can not be null !");
            setParam(ps, index, value);
            index++;
        }
        if (index == 0) throw new RuntimeException("There is no Column annotation in class or not define primary key!");
    }
    
    public <T extends BasicEntity> void updatePKCache(T obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Entity en = obj.getClass().getAnnotation(Entity.class);
        String table = en.name();
        EntityClass ec = EntityManager.getEntityClass(table);
        for (String col : ec.getPkColumns()){
            FieldClass f = ec.getColumnFieldClass(col);
            Object value = f.getter.invoke(obj);
            obj.setId(col, value);
        }
    }
    
    public void setParam(PreparedStatement ps, int columnIdx, Object value) throws SQLException {
        if (value == null) {
            ps.setObject(columnIdx, null);
        } else {
            if (debug) System.out.println(columnIdx + ": " + value);
            if (value instanceof String){
                ps.setString(columnIdx, (String) value);
            } else if (value instanceof Boolean) {
                ps.setBoolean(columnIdx, (Boolean) value);
            } else if (value instanceof Date) {
                ps.setTimestamp(columnIdx, new Timestamp(((Date) value).getTime()));
            } else if (value instanceof Integer) {
                ps.setInt(columnIdx, (Integer) value);
            } else if (value instanceof Long) {
                ps.setLong(columnIdx, (Long) value);
            } else if (value instanceof Double) {
                ps.setDouble(columnIdx, (Double) value);
            } else if (value instanceof Timestamp) {
                ps.setTimestamp(columnIdx, (Timestamp) value);
            } else if (value instanceof BigDecimal) {
                ps.setBigDecimal(columnIdx, (BigDecimal) value);
            } else if (value instanceof Clob) {
                ps.setClob(columnIdx, (Clob) value);
            } else {
                throw new SQLException("Data type " + value.getClass() + " is invalidate, it should be either boolean, date, integer, long, timestamp, bigDecimal or double. ");
            }
        }
    }
    
    public String createSelectParamsSyntax(String table) throws ClassNotFoundException {
        StringBuilder result = new StringBuilder();
        for (String col : getEntityClass(table).getColumns()){
            result.append(col + ", ");
        }
        String params = result.toString();
        if (params == null || params.equals("")) throw new RuntimeException("There is no Column annotation in class or not define primary key!");
        return params;
    }
    
    public String createSelectStatement(String table) throws ClassNotFoundException {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        String selectParams = createSelectParamsSyntax(table);
        if (selectParams.endsWith(", ")){
            sql.append(selectParams.substring(0, selectParams.length() - 2));
        } else {
            sql.append(selectParams);
        }
        sql.append(" FROM " + table);
        return sql.toString();
    }

    public String createSelectSql(String table, boolean WherePK) throws ClassNotFoundException {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        String selectParams = createSelectParamsSyntax(table);
        if (selectParams.endsWith(", ")){
            sql.append(selectParams.substring(0, selectParams.length() - 2));
        } else {
            sql.append(selectParams);
        }
        sql.append(" FROM " + table);
        if (WherePK) {
            sql.append(" WHERE " + createWherePKSyntax(getEntityClass(table).getPkColumns()));
        } else {
            sql.append(" WHERE " + createWhereAllSyntax(getEntityClass(table).getPkColumns()));
        }
        return sql.toString();
    }
    
    public String createSelectSql(Object obj, EntityClass ec, List<String> tables) throws Exception{
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        String selectParams = createSelectParamsSyntax(ec, tables);
        if (selectParams.endsWith(", ")){
            sql.append(selectParams.substring(0, selectParams.length() - 2));
        } else {
            sql.append(selectParams);
        }
        sql.append(" " + createTableSql(ec, tables));
        sql.append(" WHERE " + createWhereSyntax(obj, tables));
        return sql.toString();
    }
    
    private String createTableSql(EntityClass ec, List<String> tables) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM " + ec.getTable() + " T1, ");
        for (int i = 0; i < tables.size(); i++){
            if (i == tables.size() - 1){
                sql.append(tables.get(i) + " " + "T" + (i + 2));
            } else {
                sql.append(tables.get(i) + " T" + (i + 2) + ", ");
            }
        }
        String result = sql.toString().trim();
        if (result.endsWith(",")) result = result.substring(0, result.length() - 1);
        return result;
    }

    private String createSelectParamsSyntax(EntityClass ec, List<String> tables) {
        StringBuilder bud = new StringBuilder();
        for (String column : ec.getColumns()){
            bud.append("T1." + column + ", ");
        }
        int counter = 2;
        for (String table : tables){
            for (String column : getEntityClass(table).getColumns()){
                bud.append("T" + counter + "." + column + ", ");
            }
            counter++;
        }
        String result = bud.substring(0, bud.length() - 2);
        return result;
    }

    protected List<String> findAllTables(Object obj, EntityClass ec, List<String> tables) throws Exception {
        for (FieldClass f : getFieldClass(obj.getClass().getName())){
            Object value = invokeGetter(f, obj);
            if (value != null && f._1to1 != null) {
                String table = f._1to1_cols.value()[0].table();
                tables.add(table);
                EntityClass _ec = getEntityClass(table);
                findAllTables(value, _ec, tables);
            }
        }
        return tables;
    }

    private String createWhereSyntax(Object obj, List<String> tables) throws Exception {
        StringBuilder bud = new StringBuilder();
        for (FieldClass f : getFieldClass(obj.getClass().getName())){
            Object value = invokeGetter(f, obj);
            if (value != null && f.c != null){
                bud.append("T1." + f.c.name() + " = ? and ");
            }
            if (value != null && f._1to1 != null){
                for (JoinColumn jc : f._1to1_cols.value()){
                    int tableIdx = getTableIndex(tables, jc.table());
                    bud.append("T1." + jc.name() + " = T" + tableIdx + "." + jc.referencedColumnName() + " and ");
                    createWhereSyntax(value, tableIdx, bud, tables);
                }
            }
        }
        String result = bud.toString();
        if (!result.equals("")) result = result.substring(0, result.length() - 4);
        return result;
    }

    private void createWhereSyntax(Object obj, int tableIdx, StringBuilder bud, List<String> tables) throws Exception {
        for (FieldClass f : getFieldClass(obj.getClass().getName())){
            Object value = invokeGetter(f, obj);
            if (value != null && f.c != null){
                bud.append("T" + tableIdx + "." + f.c.name() + " = ? and ");
            }
            if (value != null && f._1to1 != null){
                for (JoinColumn jc : f._1to1_cols.value()){
                    int _tableIdx = getTableIndex(tables, jc.table());
                    bud.append("T" + tableIdx + "." + jc.name() + " = T" + _tableIdx + "." + jc.referencedColumnName() + " and ");
                    createWhereSyntax(value, _tableIdx, bud, tables);
                }
            }
        }
    }

    private int getTableIndex(List<String> tables, String table) {
        int i = 2;
        for (String t : tables) {
            if (t.equalsIgnoreCase(table)){
                return i;
            }
            i++;
        }
        throw new RuntimeException("can not find " + table + " in table list!");
    }
    
    public <T extends BasicEntity> T constructObject(Class<T> objClass, EntityClass ec, ResultSet rs) throws Exception{
        int startIdx = 1;
        T obj = objClass.newInstance();
        for (FieldClass f : getFieldClass(objClass.getName())) {
            if (f.c != null) {
                Object value = rs.getObject(startIdx);
                invokeSetter(f, obj, value);
                startIdx++;
            }
        }
        for (FieldClass f : getFieldClass(ec.getClassName())){
            if (f._1to1 != null && f._1to1.fetch() == FetchType.EAGER){
                setKeyMapping(obj, f);
            }
            if (f._1toM != null && f._1toM.fetch() == FetchType.EAGER){
                setKeyMapping(obj, f);
            }
        }
        for (String pk : ec.getPkColumns()) {
            FieldClass fCls = ec.getColumnFieldClass(pk);
            Object pkValue = fCls.getter.invoke(obj);
            obj.setId(pk, pkValue);
        }
        return obj;
    }
    
    private boolean isPrimaryKeySearch(EntityClass ec, JoinColumns joinCols) {
        for (String pkCol : ec.getPkColumns()) {
            boolean found = false;
            for (JoinColumn jc : joinCols.value()) {
                if (jc.name().equals(pkCol)) {
                    found = true;
                }
            }
            if (found == false) return false; 
        }
        return true;
    }

    public void setKeyMapping(Object obj, FieldClass[] fields) throws Exception{
        for (FieldClass f : fields){
            setKeyMapping(obj, f);
        }
    }
    
    public void setKeyMapping(Object obj, FieldClass field) throws Exception{
        Entity en = obj.getClass().getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        if (field._1toM != null){
            EntityClass _ec = getEntityClass(field._1toM_cols.value()[0].table());
            Object dao = createDAO(_ec.getDaoClassName());
            Object search = createObj(_ec.getClassName());
            for (JoinColumn jc : field._1toM_cols.value()){
                FieldClass _1to1F = ec.getColumnFieldClass(jc.name());
                Object value = invokeGetter(_1to1F, obj);
                FieldClass searchF = _ec.getColumnFieldClass(jc.referencedColumnName());
                invokeSetter(searchF, search, value);
            }
            Object _obj = invokeMethod("find", 1, dao, search);
            if (_obj instanceof List){
                List<?> list = (List<?>) _obj;
                invokeSetter(field, obj, list);
            }
        }
        if (field._1to1 != null){
            EntityClass _ec = getEntityClass(field._1to1_cols.value()[0].table());
            Object dao = createDAO(_ec.getDaoClassName());
            Object search = createObj(_ec.getClassName());
            for (JoinColumn jc : field._1to1_cols.value()){
                FieldClass _1to1F = ec.getColumnFieldClass(jc.name());
                Object value = invokeGetter(_1to1F, obj);
                FieldClass searchF = _ec.getColumnFieldClass(jc.referencedColumnName());
                invokeSetter(searchF, search, value);
            }
            if (isPrimaryKeySearch(_ec, field._1to1_cols)) {
                Object _obj = invokeMethod("findByPrimaryKey", 1, dao, search);
                invokeSetter(field, obj, _obj);
            } else {
                Object _obj = invokeMethod("find", 1, dao, search);
                if (_obj instanceof List){
                    List<?> list = (List<?>) _obj;
                    if (list != null && list.size() == 1) {
                        invokeSetter(field, obj, list.get(0));
                    }
                }
            }
        }
    }
    
    private Object invokeMethod(String method, int args, Object obj1, Object... obj2) throws Exception{
        for (Method m : obj1.getClass().getMethods()){
            if (m.getName().equals(method) && m.getParameterTypes().length == args){
                return m.invoke(obj1, obj2);
            }
        }
        throw new RuntimeException(method + " method not found!");
    }
    
    private Object createDAO(String daoClassName) throws Exception{
        Object dao = Class.forName(daoClassName).newInstance();
        EntityManager mgr = new EntityManager(getDataSource() ,getConnection());
        invokeMethod("setEntityManager", 1, dao, mgr);
        return dao;
    }
    
    private Object createObj(String className){
        try {
            return Class.forName(className).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    
    private Object invokeGetter(FieldClass f, Object obj) throws Exception {
        try {
            return f.getter.invoke(obj);
        } catch (Exception e) {
            System.out.print("<" + obj.getClass().getName() + "> " + f.f.getName() + " getter fails! ");
            throw new Exception(e);
        }
    }
    
    private void invokeSetter(FieldClass f, Object obj, Object value) throws Exception {
        if (value == null) return; 
        Type[] types = f.setter.getParameterTypes();
        if (types.length != 1) {
            throw new Exception("setter can has only one parameter");
        }
        Class<?> cls = ObjectConverter.getClass(types[0]);
        if (cls != obj.getClass()) {
            NumberConverter numConverter = new NumberConverter();
            if (numConverter.isNumber(types[0])) {
                value = numConverter.Convert(value, cls);
            }
            TimeConverter tConverter = new TimeConverter();
            if (tConverter.isTime(types[0])) {
                value = tConverter.Convert(value, cls);
            }
            if (value == Clob.class) {
                ClobConverter cConverter = new ClobConverter();
                value = cConverter.Convert(value, String.class);
            }
            if (value.getClass() == String.class) {
                if (ObjectConverter.getClass(types[0]) == Clob.class) {
                    ClobConverter cConverter = new ClobConverter();
                    value = cConverter.Convert(value, Clob.class);
                }
            }
        }
        try {
            f.setter.invoke(obj, value);
        } catch (Exception e) {
            System.out.println(f.f.getName() + " set " + value + " fail! ");
            System.out.println("<" + obj.getClass().getName() + "> " + f.f.getName() + " set " + value + " fail! ");
            System.out.println("field type:" + f.f.getType() + ", value type: " + value.getClass().getName() + " fail! ");
            throw new Exception(e);
        }
    }
    
    public static Date getDate(int year, int month, int day, int hour, int min, int sec) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day - 1);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);
        cal.set(Calendar.SECOND, sec);
        return cal.getTime();
    }
    
    public static void printObject(Object obj, String indent) throws Exception{
        if (obj != null){
            System.out.println(indent + "<" + obj.getClass().getName() + ">---------------------------------------");
            Class<?> cls = obj.getClass();
            while (cls != null){
                Field[] fields = cls.getDeclaredFields();
                for (Field f : fields){
                    Column c = f.getAnnotation(Column.class);
                    OneToOne _1to1 = f.getAnnotation(OneToOne.class);
                    OneToMany _1toM = f.getAnnotation(OneToMany.class);
                    if (c != null){
                        System.out.println("    " + indent + f.getName() + " = " + runGetter(f, obj));
                    }
                    if (_1to1 != null){
                        System.out.print("    " + indent + f.getName());
                        printObject(runGetter(f, obj), indent + "    ");
                    }
                    if (_1toM != null){
                        List<?> list = (List<?>) runGetter(f, obj);
                        if (list != null){
                            for (Object akObj : list){
                                printObject(akObj, indent + "    ");
                            }    
                        }
                    }
                }
                cls = cls.getSuperclass();
            }
            System.out.println(indent + "---------------------------------------");
        }
    }
    
    public static boolean include(String[] samples, String name) {
        if (samples == null || samples == null) return false;
        for (String str : samples){
            if (str.equalsIgnoreCase(name)) return true;
        }
        return false;
    }
    
    public static void debugTable(Object printerTool) {
        DebugPrinterTool printer = new DebugPrinterTool(printerTool);
        if (tableMap.size() == 0){
            if (printer != null) printer.printMarkMessage("warning!!!  cache table is empty");
        }
        for (String table : tableMap.keySet()){
            if (printer != null) printer.printObjectParam("table", table);
            if (printer != null) printer.printObjectParam(tableMap.get(table));
        }
    }
    
    public static String replaceAll(String originalText, String key, String replaceValue){
        if (originalText == null || originalText.length() == 0 || key == null || key.length() == 0 ||
                replaceValue == null) return originalText;
        int fromIndex = 0, idx, len = key.length();
        StringBuffer sb = new StringBuffer();
        boolean found = false;
        while ((idx = originalText.indexOf(key, fromIndex)) != -1) {
            if (!found) found = true;
            sb.append(originalText.substring(fromIndex, idx));
            sb.append(replaceValue);
            fromIndex = idx + len;
        }
        if (!found) return originalText;
        
        if (fromIndex < originalText.length()) {
            sb.append(originalText.substring(fromIndex));
        }
        return sb.toString();
    }
    
    public static <T extends BasicEntity> T construct(Object source, Class<T> target) throws Exception {
        T result = null;
        if (source == null) return null;
        try {
            result = target.newInstance();
            List<String> clsTFieldNames = new ArrayList<String>();
            Class<?> cls = target;
            // print field
            while (cls != null && cls != BasicEntity.class) {
                Field[] fields = cls.getDeclaredFields();
                for (Field f : fields) {
                    clsTFieldNames.add(f.getName());
                }
                cls = cls.getSuperclass();
            }
            for (String name : clsTFieldNames) {
                Object value = runGetter(source, name);
                if (value != null) invokeSetter(result, name, value);
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
        return result;
    }
    
    private static void invokeSetter(Object obj, String fieldName, Object value) throws Exception {
        String methodName = "set" + fieldName;
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().toLowerCase().equals(methodName.toLowerCase()) && method.getParameterTypes().length == 1)) {
                Class<?> type = method.getParameterTypes()[0];
                if (type.getName().equalsIgnoreCase(value.getClass().getName())) {
                    method.invoke(obj, value);
                }
                break;
            }
        }
    }
    
    public static <V> V setup(Class<V> clsV, ResultSet resultSet) {
        try {
            V obj = clsV.newInstance();
            List<Field> fields = new ArrayList<Field>();
            List<String> columnNames = new ArrayList<String>();
            Class<?> cls = clsV;
            while (cls != Object.class) {
                for (Field f : cls.getDeclaredFields()) {
                    Column c = f.getAnnotation(Column.class);
                    ColumnAlias ca = f.getAnnotation(ColumnAlias.class);
                    if (c != null) {
                        fields.add(f);
                        columnNames.add(c.name());
                    } else if (ca != null) {
                        fields.add(f);
                        columnNames.add(ca.name());
                    }
                }
                cls = cls.getSuperclass();
            }
            ResultSetMetaData meta = resultSet.getMetaData();
            HashSet<String> columnLabels = new HashSet<String>();
            for (int i = 0; i < meta.getColumnCount(); i++) {
                String label = meta.getColumnLabel(i + 1);
                columnLabels.add(label.toLowerCase());
            }
            for (int i = 0; i < fields.size(); i++) {
                String column = columnNames.get(i);
                Field f = fields.get(i);
                Type fType = f.getType();
                Class<?> clsF = ObjectConverter.getClass(fType);
                if (columnLabels.contains(column.toLowerCase())) {
                    Object value = resultSet.getObject(column);
                    if (value != null) {
                        if (clsF == String.class) {
                            value = value.toString();
                        } else if (nConverter.isNumber(fType)) {
                            value = nConverter.Convert(value, clsF);
                        } else if (tConverter.isTime(fType)) {
                            value = tConverter.Convert(value, clsF);
                        }
                        PlainObjectOperator.runSetter(obj, f.getName(), value);
                    }    
                }
            }
            return obj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    protected Map<String, Object> getMap(ResultSet resultSet) throws SQLException {
        Map<String, Object> result = new CaseInsensitiveMap();
        ResultSetMetaData meta = resultSet.getMetaData();
        HashSet<String> columnLabels = new HashSet<String>();
        for (int i = 0; i < meta.getColumnCount(); i++) {
            String label = meta.getColumnLabel(i + 1);
            columnLabels.add(label.toLowerCase());
        }
        for (String label : columnLabels) {
            Object value = resultSet.getObject(label);
            result.put(label, value);
        }
        return result;
    }
    
    
}