package com.yung.jpa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.*;
import javax.sql.DataSource;

public class EntityManager extends BasicEntityManager {
    
    private boolean DEBUG = false;
    
    public EntityManager(DataSource dataSource, Connection conn){
        this.setDataSource(dataSource);
        this.setConnection(conn);
    }
    
    public <T extends BasicEntity> void insert(T obj) throws Exception {
        checkEntityAnnotation(obj.getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createInsertSql(obj.getClass());
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            setParameters(ps, obj);
            ps.executeUpdate();
            updatePKCache(obj);
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> void update(T obj) throws Exception {
        checkEntityAnnotation(obj.getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createUpdateSql(obj.getClass());
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            int index = setParameters(ps, obj);
            setPrimaryKeyParameters(index, ps, obj);
            ps.executeUpdate();
            updatePKCache(obj);
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> void update(T obj, String... fields) throws Exception {
        checkEntityAnnotation(obj.getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createUpdateSql(obj.getClass(), fields);
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            int index = setParameters(ps, obj, fields);
            setPrimaryKeyParameters(index, ps, obj);
            ps.executeUpdate();
            updatePKCache(obj);
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> void delete(T obj) throws Exception {
        checkEntityAnnotation(obj.getClass());
        PreparedStatement ps = null;
        try {
            prepareConnection();
            String sql = createDeleteSql(obj.getClass());
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            @SuppressWarnings("unchecked")
            T key = (T) construct(obj, obj.getClass());
            updatePKCache(key);
            setPrimaryKeyParameters(ps, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    public <T extends BasicEntity> T findByPrimaryKey(Class<T> objClass, T obj) throws Exception {
        checkEntityAnnotation(objClass);
        Entity en = objClass.getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            prepareConnection();
            String sql = createSelectSql(en.name(), true);
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            setPrimaryKeyParameters(ps, obj);
            rs = ps.executeQuery();
            T result = null;
            if (rs.next()) {
                result = constructObject(objClass, ec, rs);
            }
            return result;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> List<T> listAll(Class<T> objClass) throws Exception {
        checkEntityAnnotation(objClass);
        Entity en = objClass.getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        List<T> results = new ArrayList<T>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            prepareConnection();
            String sql = createSelectSql(ec.getTable(), false);
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            T result = null;
            while (rs.next()) {
                result = constructObject(objClass, ec, rs);
                results.add(result);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public <T extends BasicEntity> List<T> find(Class<T> objClass, String whereSql, Object... params) throws Exception {
        checkEntityAnnotation(objClass);
        Entity en = objClass.getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        List<T> results = new ArrayList<T>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            prepareConnection();
            String sql = createSelectStatement(ec.getTable());
            whereSql = compileWhereSql(objClass, whereSql);
            sql = sql + " " + whereSql;
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            rs = ps.executeQuery();
            T result = null;
            while (rs.next()) {
                result = constructObject(objClass, ec, rs);
                results.add(result);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    private <T extends BasicEntity> String compileWhereSql(Class<T> cls, String whereSql) {
        for (FieldClass f : getFieldClass(cls.getName())){
            String name = f.f.getName();
            if (f.c != null) {
                String columnName = f.c.name();
                int start = 0;
                int idx = whereSql.indexOf(name, start);
                while (idx > 0) {
                    if (whereSql.charAt(idx + name.length()) == ' ') {
                        whereSql = whereSql.substring(0, idx) + columnName + whereSql.substring(idx + name.length());
                        start = idx + columnName.length();
                    } else {
                        start = whereSql.indexOf(" ", idx);
                    }
                    idx = whereSql.indexOf(name, start);
                }
            }
        }
        return whereSql;
    }

    public <T extends BasicEntity> List<T> find(Class<T> objClass, T obj) throws Exception {
        checkEntityAnnotation(objClass);
        Entity en = objClass.getAnnotation(Entity.class);
        EntityClass ec = getEntityClass(en.name());
        List<T> results = new ArrayList<T>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            prepareConnection();
            List<String> tables = new ArrayList<String>();
            findAllTables(obj, ec, tables);
            String sql = createSelectSql(obj, ec, tables);
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            setParameters(ps, obj, ec, tables);
            rs = ps.executeQuery();
            while (rs.next()) {
                T result = constructObject(objClass, ec, rs);
                if (result != null) results.add(result);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
    public void flush(){
        try {
            if (connection != null){
                connection.commit();
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public Connection prepareConnection() throws SQLException{
        if (connection == null) {
            setConnection(getDataSource().getConnection());
            connection = getConnection();
            connection.setAutoCommit(false);
        }
        return connection;
    }

    public <T> List<T> findBySql(Class<T> cls, String sql, Object[] params) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<T> results = new ArrayList<T>();
            prepareConnection();
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                T obj = setup(cls, rs);
                results.add(obj);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }

    public List<Map<String, Object>> findBySql(String sql, Object[] params) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
            prepareConnection();
            if (DEBUG) System.out.println(sql);
            ps = connection.prepareStatement(sql);
            int idx = 1;
            for (Object param : params) {
                setParam(ps, idx, param);
                idx++;
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                Map<String, Object> map = getMap(rs);
                results.add(map);
            }
            return results;
        } catch (SQLException e) {
            throw new SQLException(e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
    }
    
}