package com.yung.jpa;

import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.ParameterizedType;

import javax.sql.DataSource;

public abstract class AbstractDAO<T extends BasicEntity> implements IntefaceDAO<T>{
    
    private Class<T> persistentClass;
    
    EntityManager mgr;
    
    @SuppressWarnings("unchecked")
    protected AbstractDAO(){
        ParameterizedType cls = (ParameterizedType) getClass().getGenericSuperclass();
        persistentClass = (Class<T>) cls.getActualTypeArguments()[0];
    }
    
    public Class<T> getType(){
        return persistentClass;
    }
    
    protected AbstractDAO(DataSource ds){
        mgr = new EntityManager(ds, null);
    }
    
    public Class<?> getDAOType(){
        return getClass();
    }
    
    public void insert(T object) throws Exception {
        mgr.insert(object);
    }
    
    public void update(T object) throws Exception {
        if (object.isEmptyCacheId()) {
            mgr.updatePKCache(object);
        }
        mgr.update(object);
    }
    
    public void update(T object, String... fields) throws Exception {
        if (object.isEmptyCacheId()) {
            mgr.updatePKCache(object);
        }
        mgr.update(object, fields);
    }

    public void delete(T object) throws Exception {
        if (object.isEmptyCacheId()) {
            mgr.updatePKCache(object);
        }
        mgr.delete(object);
    }

    public List<T> find(T object) throws Exception {
        List<T> results = new ArrayList<T>();
        results = mgr.find(getType(), object);
        return results;
    }

    public List<T> find(String whereSql, Object... params) throws Exception {
        List<T> results = new ArrayList<T>();
        results = mgr.find(getType(), whereSql, params);
        return results;
    }
    
    public T findOne(String whereSql, Object... params) throws Exception {
        List<T> results = mgr.find(getType(), whereSql, params);
        if (results != null &&  results.size() > 0) {
            return results.get(0);
        }
        return null;
    }
    
    public T findByPrimaryKey(T object) throws Exception {
        if (object.isEmptyCacheId()) {
            mgr.updatePKCache(object);
        }
        T result = mgr.findByPrimaryKey(getType(), object);
        return result;
    }
    
    public List<T> listAll() throws Exception {
        List<T> results = mgr.listAll(getType());
        return results;
    }

    public void setEntityManager(EntityManager mgr) {
        this.mgr = mgr;
    }

    public void flush() {
        mgr.flush();
    }

    public void setKeyMapping(T object) throws Exception {
        mgr.setKeyMapping(object, BasicEntityManager.getFieldClass(getType().getName()));
    }
    
    public void setKeyMapping(List<T> objects) throws Exception {
        for (T object : objects) {
            mgr.setKeyMapping(object, BasicEntityManager.getFieldClass(getType().getName()));
        }
    }
    
    public void insert(List<T> list) throws Exception {
        for (T t : list) {
            insert(t);
        }
    }
    
    public void update(List<T> list) throws Exception {
        for (T t : list) {
            update(t);
        }
    }
    
    public void delete(List<T> list) throws Exception {
        for (T t : list) {
            delete(t);
        }
    }

    public void deleteBySql(String whereSql, Object... params) throws Exception {
        List<T> list = find(whereSql, params);
        delete(list);
    }
    
}