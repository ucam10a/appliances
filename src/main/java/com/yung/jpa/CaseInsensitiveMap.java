package com.yung.jpa;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CaseInsensitiveMap implements Map<String, Object> {

    Map<String, Object> rawMap;
    
    public CaseInsensitiveMap() {
        rawMap = new HashMap<String, Object>();
    }

    public int size() {
        return rawMap.size();
    }

    public boolean isEmpty() {
        return rawMap.isEmpty();
    }

    public boolean containsKey(Object key) {
        if (key != null) {
            key = key.toString().toLowerCase();
        }
        return rawMap.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return rawMap.containsValue(value);
    }

    public Object get(Object key) {
        if (key != null) {
            key = key.toString().toLowerCase();
        }
        return rawMap.get(key);
    }

    public Object put(String key, Object value) {
        if (key != null) {
            key = key.toString().toLowerCase();
        }
        return rawMap.put(key, value);
    }

    public Object remove(Object key) {
        if (key != null) {
            key = key.toString().toLowerCase();
        }
        return rawMap.remove(key);
    }

    public void putAll(Map<? extends String, ? extends Object> m) {
        if (m != null && m.size() > 0) {
            for (java.util.Map.Entry<? extends String, ? extends Object> entry : m.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (key != null) {
                    key = key.toLowerCase();
                }
                rawMap.put(key, value);
            }
        }
    }

    public void clear() {
        rawMap.clear();
    }

    public Set<String> keySet() {
        return rawMap.keySet();
    }

    public Collection<Object> values() {
        return rawMap.values();
    }

    public Set<java.util.Map.Entry<String, Object>> entrySet() {
        return rawMap.entrySet();
    }
    
}