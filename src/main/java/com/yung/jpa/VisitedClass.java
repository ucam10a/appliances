package com.yung.jpa;

class VisitedClass implements Visitable<ClassVisitor>{

    VisitedClass(){
    }
    
    VisitedClass(String className){
        this.className = className;
    }
    
    private String className;
    
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
    
    public void accept(ClassVisitor visitor) {
        visitor.visit(this);
    }
    
    @Override
    public String toString(){
        return className;
    }
    
    @Override
    public boolean equals(Object obj){
        if (obj instanceof VisitedClass){
            VisitedClass cls = (VisitedClass) obj;
            if (cls.className.equals(className)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}
