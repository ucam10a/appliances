package com.yung.access.util;

import java.util.Vector;

public class MesgBox {

    private String message;
    
    public MesgBox(String msg) {
        this.setMessage(msg);
    }
    
    public static Vector<MesgBox> setVecMessage(String errormsg) {
        Vector<MesgBox> msgVector = new Vector<MesgBox>();
        MesgBox msgbox = new MesgBox(errormsg);
        msgVector.add(msgbox);
        return msgVector;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
