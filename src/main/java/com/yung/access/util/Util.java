package com.yung.access.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Util {

    public static String getMd5(String input) throws NoSuchAlgorithmException{
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(input.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);
        // Now we need to zero pad it if you actually want the full 32 chars.
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }
        return hashtext;
    }
    
    public static Timestamp getTodayTimestamp(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTimeInMillis());
    }

    public static Timestamp getTimestamp(String day) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(day);
        return new Timestamp(date.getTime());
    }
    
    public static double halfRoundUp(int scale, double input){
    	double b = new BigDecimal(input + "").setScale(scale, BigDecimal.ROUND_HALF_DOWN).doubleValue();
    	return b;
    }
    
    public static String replaceAll(String originalText, String key, String replaceValue){
        if (originalText == null || originalText.length() == 0 || key == null || key.length() == 0 ||
                replaceValue == null) return originalText;
        int fromIndex = 0, idx, len = key.length();
        StringBuffer sb = new StringBuffer();
        boolean found = false;
        while ((idx = originalText.indexOf(key, fromIndex)) != -1) {
            if (!found) found = true;
            sb.append(originalText.substring(fromIndex, idx));
            sb.append(replaceValue);
            fromIndex = idx + len;
        }
        if (!found) return originalText;
        
        if (fromIndex < originalText.length()) {
            sb.append(originalText.substring(fromIndex));
        }
        return sb.toString();
    }
    
}