package com.yung.access.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Properties;

import com.yung.access.quartz.ReloadMdb;
import com.yung.debug.FileUtil;

public class ToolConfig {

    private static String DBURL;
    
    private static String PASS;
    
    private static String SERVER_HOME;
    
    static {
        String CATALINA_HOME = System.getProperty("catalina.base");
        if (CATALINA_HOME == null || "".equals(CATALINA_HOME)) {
            String JBOSS_HOME = System.getProperty("jboss.home.dir");
            SERVER_HOME = JBOSS_HOME;
        } else {
            SERVER_HOME = CATALINA_HOME;
        }
    }
    
    /**
     * set all property
     */
    private static void setProperty() {

        ClassLoader loader = ToolConfig.class.getClassLoader();
        try {

            Properties prop = new Properties();
            prop.load(loader.getResourceAsStream("ToolConfig.properties"));
            // get property and set value
            //setDBURL(prop.getProperty("DBURL"));
            File dbFile = FileUtil.getFile(SERVER_HOME + "/db.mdb");
            if (!dbFile.exists()) {
                ReloadMdb reload = new ReloadMdb();
                reload.load();
            }
            String copyMdbPath = FileUtil.getFile(SERVER_HOME + "/db.mdb").getAbsolutePath();
            setDBURL(copyMdbPath);
            setPASS(prop.getProperty("PASS"));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void createFile(String filepath){
        File file = new File(filepath);
        BufferedWriter writer = null;
        try {
            //create a temporary file
            writer = new BufferedWriter(new FileWriter(file));
            writer.write("1");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
    }

    public static String getDBURL() {
        if (DBURL == null) setProperty();
        return DBURL;
    }

    public static void setDBURL(String dBURL) {
        DBURL = dBURL;
    }

    public static String getPASS() {
        if (PASS == null) setProperty();
        return PASS;
    }

    public static void setPASS(String pASS) {
        PASS = pASS;
    }
    
    public static String getServerHome() {
        if (SERVER_HOME == null || "".equals(SERVER_HOME)) {
            String CATALINA_HOME = System.getProperty("catalina.base");
            if (CATALINA_HOME == null || "".equals(CATALINA_HOME)) {
                String JBOSS_HOME = System.getProperty("jboss.home.dir");
                SERVER_HOME = JBOSS_HOME;
            } else {
                SERVER_HOME = CATALINA_HOME;
            }
        }
        return SERVER_HOME;
    }
    
}