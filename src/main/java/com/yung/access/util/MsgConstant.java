package com.yung.access.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class MsgConstant {
    
    private static String MSG_LOGIN_ERROR = null;
    
    public static String MSG_NO_ACCOUNT = "No such account!";
    
    private static String NO_DATA_CH = null;
    
    private static String DATE_FORMAT_ERROR = null;
    
    private static String NUMBER_FORMAT_ERROR = null;
    
    public static final String NO_DATA = "no data";

    private static final String MSG_NO_ACCOUNT_CH = null;
    
    private static Map<String, String> chMsgMap;
    
    private static void setProp(){
        ClassLoader loader = ToolConfig.class.getClassLoader();
        try {

            Properties prop = new Properties();
            prop.load(loader.getResourceAsStream("ToolConfig.properties"));
            // get property and set value
            setNO_DATA_CH(prop.getProperty("noData.ch"));
            setDATE_FORMAT_ERROR(prop.getProperty("date.error.ch"));
            setNUMBER_FORMAT_ERROR(prop.getProperty("number.error.ch"));
            setMSG_LOGIN_ERROR(prop.getProperty("login.first"));
            chMsgMap = new HashMap<String, String>();
            chMsgMap.put("add.new.fund", prop.getProperty("add.new.fund"));
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static String getNO_DATA_CH() {
        if (NO_DATA_CH == null) setProp();
        return NO_DATA_CH;
    }

    public static void setNO_DATA_CH(String nO_DATA_CH) {
        NO_DATA_CH = nO_DATA_CH;
    }

    public static String getDATE_FORMAT_ERROR() {
        if (DATE_FORMAT_ERROR == null) setProp();
        return DATE_FORMAT_ERROR;
    }

    public static void setDATE_FORMAT_ERROR(String dATE_FORMAT_ERROR) {
        DATE_FORMAT_ERROR = dATE_FORMAT_ERROR;
    }

    public static String getNUMBER_FORMAT_ERROR() {
        if (NUMBER_FORMAT_ERROR == null) setProp();
        return NUMBER_FORMAT_ERROR;
    }

    public static void setNUMBER_FORMAT_ERROR(String nUMBER_FORMAT_ERROR) {
        NUMBER_FORMAT_ERROR = nUMBER_FORMAT_ERROR;
    }

    public static Map<String, String> getChMsgMap() {
        if (chMsgMap == null) setProp();
        return chMsgMap;
    }

    public static String getMsgNoAccountCh() {
        if (chMsgMap == null) setProp();
        return MSG_NO_ACCOUNT_CH;
    }

    public static String getMSG_PASSWORD_ERROR() {
        if (chMsgMap == null) setProp();
        return chMsgMap.get("pass.error");
    }

    public static String getMSG_LOGIN_ERROR() {
        if (chMsgMap == null) setProp();
        return MSG_LOGIN_ERROR;
    }

    public static void setMSG_LOGIN_ERROR(String mSG_LOGIN_ERROR) {
        MSG_LOGIN_ERROR = mSG_LOGIN_ERROR;
    }

}