package com.yung.access;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.List;

import com.yung.access.entity.Customer;
import com.yung.access.entity.CustomerDAO;
import com.yung.access.entity.Maintenance;
import com.yung.access.entity.MaintenanceDAO;
import com.yung.access.entity.Sold;
import com.yung.access.entity.SoldDAO;
import com.yung.access.transaction.JDBCDataSource;
import com.yung.debug.ConsoleDebugPrinter;
import com.yung.jpa.DAOFactory;

public class Test {

    public static void main(String args[]) throws Exception {
        
        ConsoleDebugPrinter printer = new ConsoleDebugPrinter();
        
        String url = "E:/git/bitbucket/appliances/apache-tomcat-7.0.42/db.mdb";
        Connection conn = JDBCDataSource.getConnection(url);

        DatabaseMetaData md = conn.getMetaData();
        ResultSet rs = md.getTables(null, null, "%", null);
        while (rs.next()) {
            System.out.println(rs.getString(3));
        }
        
        String phone = "3415552";
        
//        CustomerDAO dao1 = (CustomerDAO) DAOFactory.getDAO(Customer.class, conn);
//        Customer customer = new Customer();
//        customer.setPhone(phone);
//        List<Customer> list1 = dao1.find(customer);
//        printer.printObjectParam("list1", list1);
        
        SoldDAO dao2 = (SoldDAO) DAOFactory.getDAO(Sold.class, conn);
        List<Sold> list2 = dao2.find(" order by soldDate asc ");
        printer.printObjectParam("list2", list2);
        
//        MaintenanceDAO dao3 = (MaintenanceDAO) DAOFactory.getDAO(Maintenance.class, conn);
//        Maintenance maintenance = new Maintenance();
//        maintenance.setPhone(phone);
//        List<Maintenance> list3 = dao3.find(maintenance);
//        printer.printObjectParam("list3", list3);
        
        
    }
    
    
    
}
