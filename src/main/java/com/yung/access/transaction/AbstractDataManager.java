package com.yung.access.transaction;

import java.sql.Connection;

/**
 * The abstract data manager which is responsible for inserting, delete and update data, similar to procedure
 * 
 * @author Yung Long
 * 
 */
public abstract class AbstractDataManager implements IDataManager {

    /** sql connection */
    protected Connection conn;

    /** query manager */
    protected IQueryManager querymgr;
    
    /**
     * constructor
     * @param conn
     */
    public AbstractDataManager(Connection conn, IQueryManager querymgr) {
        this.conn = conn;
        this.querymgr = querymgr;
    }
    
}