package com.yung.access.transaction;

import java.sql.Connection;

/**
 * The class which extend from AbstractDataManager, offer a standard data manager class for some specific database
 * if some standard query does not work, you can create a new subclass and override AbstractDataManager method
 * 
 * @author Yung Long
 * 
 */
public class DataManager extends AbstractDataManager {

    public DataManager(Connection conn, IQueryManager querymgr) {
        super(conn, querymgr);
    }
    
}
