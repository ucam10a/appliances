package com.yung.access.transaction;

import java.sql.*;

/**
 * The manager is used to initialize database, import or export data.
 * 
 * @author Yung Long
 * 
 */
public abstract class AbstractTransactionFactory {
    
    /**
     * get data transaction manager
     * @param conn sql connection
     * @param querymgr the query transaction manager 
     * @return data transaction manager
     */
    public static IDataManager getDataManager(Connection conn, IQueryManager querymgr) {
        return new DataManager(conn, querymgr);
    }

    /**
     * get query transaction manager
     * @param conn
     * @return
     */
    public static IQueryManager getQueryManager(Connection conn) {
        return new QueryManager(conn);
    }
           
}
