package com.yung.access.transaction;

import java.sql.Connection;

/**
 * The class which extend from AbstractQueryManager, offer a standard query manager class, for some specific database
 * if some standard query does not work, you can create a new subclass and override AbstractQueryManager method
 * 
 * @author Yung Long
 * 
 */
public class QueryManager extends AbstractQueryManager {
    
    public QueryManager(Connection conn) {
        super(conn);
    }

}
