package com.yung.access.transaction;

import java.sql.Connection;
import java.sql.DriverManager;

public class JDBCDataSource {
    
    public static Connection getConnection(String url) {
        try {
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
            Connection conn = DriverManager.getConnection("jdbc:ucanaccess://" + url + ";lockmdb=false;ignorecase=true");
            return conn;
        } catch (Exception e) {
            System.out.println("ERROR: failed to load JDBC driver.");
            e.printStackTrace();
            return null;
        }
    }
    
}