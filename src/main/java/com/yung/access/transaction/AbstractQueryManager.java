package com.yung.access.transaction;

import java.sql.Connection;
import java.util.List;

import com.yung.access.entity.Customer;
import com.yung.access.entity.CustomerDAO;
import com.yung.access.entity.Maintenance;
import com.yung.access.entity.MaintenanceDAO;
import com.yung.access.entity.Sold;
import com.yung.access.entity.SoldDAO;
import com.yung.jpa.DAOFactory;

/**
 * The abstract query manager which is responsible for query the data from database, similar to procedure
 * 
 * @author Yung Long
 * 
 */
public abstract class AbstractQueryManager implements IQueryManager {

    /** sql connection */
    protected Connection conn;
    
    public AbstractQueryManager(Connection conn) {
        this.conn = conn;
    }
    
    public List<Customer> getCustomers(String whereSql, Object... params) throws Exception {
        CustomerDAO dao = (CustomerDAO) DAOFactory.getDAO(Customer.class, conn);
        return dao.find(whereSql, params);
    }
    
    public List<Customer> getCustomers(Customer customer) throws Exception {
        CustomerDAO dao = (CustomerDAO) DAOFactory.getDAO(Customer.class, conn);
        return dao.find(customer);
    }
    
    public List<Sold> getSolds(Sold sold) throws Exception {
        SoldDAO dao = (SoldDAO) DAOFactory.getDAO(Sold.class, conn);
        return dao.find(sold);
    }
    
    public List<Maintenance> getMaintenances(Maintenance maintenance) throws Exception {
        MaintenanceDAO dao = (MaintenanceDAO) DAOFactory.getDAO(Maintenance.class, conn);
        return dao.find(maintenance);
    }
    
    
}