package com.yung.access.transaction;

import java.util.List;

import com.yung.access.entity.Customer;
import com.yung.access.entity.Maintenance;
import com.yung.access.entity.Sold;

public interface IQueryManager {

    public List<Customer> getCustomers(String whereSql, Object... params) throws Exception;
    
    public List<Customer> getCustomers(Customer customer) throws Exception;

    public List<Sold> getSolds(Sold sold) throws Exception;

    public List<Maintenance> getMaintenances(Maintenance maintenance) throws Exception;

}
