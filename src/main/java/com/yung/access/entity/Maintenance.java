package com.yung.access.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

@Entity(name="貨品維護明細資料表")
@EntityListeners(value={MaintenanceDAO.class})
public class Maintenance extends BasicEntity {

    @Id
    @Column(name="客戶電話")
    private String phone;

    @Id
    @Column(name="服務日期")
    private Timestamp serviceDate;

    @Id
    @Column(name="機型")
    private String type;

    @Column(name="備註")
    private String comment;

    @Id
    @Column(name="服務金額")
    private Double price;

    @Column(name="服務內容")
    private String task;

    @Column(name="貨品編號")
    private String productId;

    @Column(name="識別碼")
    private String identifyId;
    
    private String serviceDateStr;

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setServiceDate(Timestamp serviceDate) {
        this.serviceDate = serviceDate;
        if (serviceDate != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            this.serviceDateStr = sdf.format(serviceDate);
        }
    }

    public Timestamp getServiceDate() {
        return serviceDate;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getTask() {
        return task;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setIdentifyId(String identifyId) {
        this.identifyId = identifyId;
    }

    public String getIdentifyId() {
        return identifyId;
    }

    public String getServiceDateStr() {
        return serviceDateStr;
    }

    public void setServiceDateStr(String serviceDateStr) {
        this.serviceDateStr = serviceDateStr;
    }
}