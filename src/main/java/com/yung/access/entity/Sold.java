package com.yung.access.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

@Entity(name="貨品售出資料明細表")
@EntityListeners(value={SoldDAO.class})
public class Sold extends BasicEntity {

    @Id
    @Column(name="客戶電話")
    private String phone;

    @Id
    @Column(name="售出日期")
    private Timestamp soldDate;

    @Id
    @Column(name="購買機型")
    private String type;

    @Column(name="備註")
    private String comment;

    @Id
    @Column(name="售出金額")
    private Double price;

    @Column(name="付款方式")
    private String payment;

    @Column(name="貨品編號")
    private String productId;
    
    private String soldDateStr;
    
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setSoldDate(Timestamp soldDate) {
        this.soldDate = soldDate;
        if (soldDate != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            this.soldDateStr = sdf.format(soldDate);
        }
    }

    public Timestamp getSoldDate() {
        return soldDate;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPayment() {
        return payment;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public String getSoldDateStr() {
        return soldDateStr;
    }

    public void setSoldDateStr(String soldDateStr) {
        this.soldDateStr = soldDateStr;
    }
}
