package com.yung.access.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

import com.yung.jpa.BasicEntity;

@Entity(name="客戶資料明細表")
@EntityListeners(value={CustomerDAO.class})
public class Customer extends BasicEntity {

    @Column(name="客戶姓名")
    private String name;

    @Column(name="客戶電話")
    @Id
    private String phone;

    @Column(name="客戶地址")
    private String address;

    @Column(name="客戶等級")
    private String level;

    @Column(name="客戶編號")
    private String id;

    @Column(name="郵遞區號")
    private Double zipCode;

    @Column(name="備註")
    private String comment;

    @Column(name="客戶手機")
    private String cellPhone;
    
    private List<Sold> solds = new ArrayList<Sold>();
    
    private List<Maintenance> maintenances = new ArrayList<Maintenance>();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setZipCode(Double zipCode) {
        this.zipCode = zipCode;
    }

    public Double getZipCode() {
        return zipCode;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public List<Sold> getSolds() {
        return solds;
    }

    public void setSolds(List<Sold> solds) {
        this.solds = solds;
    }

    public List<Maintenance> getMaintenances() {
        return maintenances;
    }

    public void setMaintenances(List<Maintenance> maintenances) {
        this.maintenances = maintenances;
    }
    
}
