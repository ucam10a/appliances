package com.yung.access.action;

import java.util.Map;

import com.yung.access.transaction.IDataManager;
import com.yung.access.transaction.IQueryManager;
import com.yung.access.util.ToolConfig;

public class Login extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = -7172267208708699023L;

    private String password;
    
    @Override
    protected boolean requireAuthenticate() {
        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected String executeAction(IQueryManager querymgr, IDataManager datamgr, @SuppressWarnings("rawtypes") Map session) throws Exception {
        
        String pass = ToolConfig.getPASS();
        if (pass.equals(password)) {
            session.put("login", "true");
            return SUCCESS;
        } else {
            session.remove("login");
            return ERROR;
        }
    
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
