package com.yung.access.action;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.yung.access.quartz.ReloadMdb;
import com.yung.access.transaction.AbstractTransactionFactory;
import com.yung.access.transaction.IDataManager;
import com.yung.access.transaction.IQueryManager;
import com.yung.access.transaction.JDBCDataSource;
import com.yung.access.util.MesgBox;
import com.yung.access.util.MsgConstant;
import com.yung.access.util.ToolConfig;

/**
 * Abstract class for all struts2 action, it is like a proxy for all action. 
 * it includes all necessary parameter and method. 
 * The main purpose for this abstract class is to implement the
 * Aspect-oriented programming design.
 * 
 * @author Yung Long
 * 
 */
public abstract class AbstractAction extends ActionSupport {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** error message vector collection */
    protected Vector<MesgBox> vecError = new Vector<MesgBox>();

    /** success message vector collection */
    protected Vector<MesgBox> vecSuccess = new Vector<MesgBox>();
    
    /** logger */
    protected static final Logger logger = LoggerFactory.getLogger(AbstractAction.class);

    /** error message for ajax */
    protected String errormsg = "";

    // use for support old IE browser
    /** json string for return */
    protected String gsonStr;
    
    /** query manager */
    protected Connection conn;
    
    /** chinese message map */
    protected Map<String, String> chMsgMap;
    
    @Override
    public String execute() {
        
        chMsgMap = MsgConstant.getChMsgMap();
        try {
            ReloadMdb reload = new ReloadMdb();
            reload.loadMdb();
            conn = JDBCDataSource.getConnection(ToolConfig.getDBURL());
            // authenticate
            if (requireAuthenticate()){
                if (authenticate(getSession()).equalsIgnoreCase(ERROR)) return "login";
            }
            IQueryManager querymgr = AbstractTransactionFactory.getQueryManager(conn);
            IDataManager datamgr = AbstractTransactionFactory.getDataManager(conn, querymgr);
            String result = executeAction(querymgr, datamgr, getSession());
            return result;
        } catch (Exception e) {
            // for IE old version
            Map<String, String> gsonMap = new HashMap<String, String>();
            Gson gson = new Gson();
            gsonMap.put("errormsg", e.toString());
            try {
                setGsonStr(URLEncoder.encode(gson.toJson(gsonMap), "ISO-8859-1"));
            } catch (UnsupportedEncodingException e1) {
                // ignore
            }
            errormsg = e.toString();
            vecError.add(new MesgBox(errormsg));
            log(this.getClass().getName(), e);
            logStackTrace(e);
            return ERROR;
        } finally {
            closeConnection();
        }
    }

    @SuppressWarnings("rawtypes")
    private Map getSession() throws Exception {
        if (ActionContext.getContext() != null){
            return ActionContext.getContext().getSession();
        }else{
            throw new Exception("error!");
        }
    }

    /**
     * require to authenticate or not
     * @return
     */
    protected abstract boolean requireAuthenticate();
    
    /**
     * real action you have to implement
     * @param session 
     * @param datamgr 
     * @param querymgr 
     * @return result string
     * @throws Exception
     */
    protected abstract String executeAction(IQueryManager querymgr, IDataManager datamgr, @SuppressWarnings("rawtypes") Map session) throws Exception;

    /**
     * check user login status, and put user information in to session
     * 
     * @return struct2 message
     */
    @SuppressWarnings("rawtypes")
    protected String authenticate(Map session) {
        if (session.get("login") != "true") {
            session.remove("login");
            setVecError(MesgBox.setVecMessage(MsgConstant.getMSG_LOGIN_ERROR()));
            return ERROR;
        }
        return "";
    }

    /**
     * get server url
     * 
     * @return server url
     */
    protected String getServerURL() {

        String serverURL = "http://localhost:8080";
        HttpServletRequest req = ServletActionContext.getRequest();
        String scheme = req.getScheme(); // http
        String serverName = req.getRemoteAddr(); // host ip
        serverURL = scheme + "://" + serverName;
        return serverURL;

    }
   
    /**
     * log class name and exception
     * 
     * @param className
     * @param e
     */
    protected void log(String className, Exception e) {
        // record error log
        logger.error(className + ", error: " + e.toString());
    }
    
    /**
     * log stack trace
     * @param e exception
     */
    protected void logStackTrace(Exception e) {
        logger.error("failed!", e );
    }
    
    /**
     * close database connection
     */
    protected void closeConnection() {
        try {
            if (conn != null) conn.close();
        } catch (SQLException e) {
            logger.error(e.toString(), e);
        }
    }
    
    public void setGsonStr(String gsonStr) {
        this.gsonStr = gsonStr;
    }

    public Vector<MesgBox> getVecError() {
        return vecError;
    }

    public void setVecError(Vector<MesgBox> vecError) {
        this.vecError = vecError;
    }

    public Vector<MesgBox> getVecSuccess() {
        return vecSuccess;
    }

    public void setVecSuccess(Vector<MesgBox> vecSuccess) {
        this.vecSuccess = vecSuccess;
    }

    public Map<String, String> getChMsgMap() {
        return chMsgMap;
    }

    public void setChMsgMap(Map<String, String> chMsgMap) {
        this.chMsgMap = chMsgMap;
    }
    
    public String returnAjaxErrormsg() {
        if (vecError.size() > 0 && errormsg.equals(""))
            errormsg = vecError.get(0).getMessage();
        return errormsg;
    }
    
}