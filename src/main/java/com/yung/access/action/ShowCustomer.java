package com.yung.access.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.yung.access.entity.Customer;
import com.yung.access.transaction.IDataManager;
import com.yung.access.transaction.IQueryManager;

public class ShowCustomer extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = -3266404475046930471L;

    private List<Customer> customers = new ArrayList<Customer>();
    
    @Override
    protected boolean requireAuthenticate() {
        return true;
    }

    @Override
    protected String executeAction(IQueryManager querymgr, IDataManager datamgr, @SuppressWarnings("rawtypes") Map session) throws Exception {
        
        List<Customer> list = querymgr.getCustomers(" where phone != ? ", "abc");
        customers.addAll(list);
        
        return SUCCESS;
        
    }
    
    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

}
