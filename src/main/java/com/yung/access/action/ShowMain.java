package com.yung.access.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yung.access.entity.Customer;
import com.yung.access.entity.Maintenance;
import com.yung.access.entity.Sold;
import com.yung.access.transaction.IDataManager;
import com.yung.access.transaction.IQueryManager;

public class ShowMain extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = -3266404475046930471L;

    private String phone;
    
    private String name;
    
    private String address;
    
    private List<Customer> customers = new ArrayList<Customer>();
    
    @Override
    protected boolean requireAuthenticate() {
        return true;
    }

    @Override
    protected String executeAction(IQueryManager querymgr, IDataManager datamgr, @SuppressWarnings("rawtypes") Map session) throws Exception {
        
        Map<String, Customer> customerMap = new HashMap<String, Customer>();
        if (notBlank(phone) || notBlank(name) || notBlank(address)) {
            String whereSql = "";
            List<Object> params = new ArrayList<Object>();
            if (notBlank(address)) {
                whereSql = getWhereSql(whereSql, "address like ?");
                params.add("%" + address + "%");
            }
            if (notBlank(phone)) {
                whereSql = getWhereSql(whereSql, "( phone = ? or cellPhone = ? )");
                params.add(phone);
                params.add(phone);
            }
            if (notBlank(name)) {
                whereSql = getWhereSql(whereSql, "name like ?");
                params.add("%" + name + "%");
            }
            List<Customer> customerList = querymgr.getCustomers(whereSql, params.toArray());
            if (customerList != null) {
                for (Customer c : customerList) {
                    customerMap.put(c.getPhone(), c);
                }
            }
            customers.addAll(customerMap.values());
            logger.info("customers size: " + customers.size());
            for (Customer c : customers) {
                Sold sold = new Sold();
                sold.setPhone(c.getPhone());
                List<Sold> soldList = querymgr.getSolds(sold);
                if (soldList != null) {
                    c.setSolds(soldList);
                }
                Maintenance maintenance = new Maintenance();
                maintenance.setPhone(c.getPhone());
                List<Maintenance> maintenanceList = querymgr.getMaintenances(maintenance);
                if (maintenanceList != null) {
                    c.setMaintenances(maintenanceList);
                }
            }
        }
        return SUCCESS;
        
    }
    
    private String getWhereSql(String originalSql, String sql){
        if (originalSql == null || originalSql.equals("")) {
            return " where " + sql;
        } else {
            return originalSql + " and " + sql;
        }
    }
    
    private boolean notBlank(String input) {
        if (input != null && !input.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
