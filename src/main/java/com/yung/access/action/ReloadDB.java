package com.yung.access.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.yung.access.quartz.ReloadMdb;
import com.yung.access.transaction.IDataManager;
import com.yung.access.transaction.IQueryManager;
import com.yung.access.util.AppZipManager;
import com.yung.access.util.MesgBox;
import com.yung.debug.FileUtil;

public class ReloadDB extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = -4660175736911327813L;
    
    private String dbUrl;
    
    private File dbFile;
    
    @Override
    protected boolean requireAuthenticate() {
        return true;
    }

    @Override
    protected String executeAction(IQueryManager querymgr, IDataManager datamgr, @SuppressWarnings("rawtypes") Map session) throws Exception {
    
        System.out.println("dbUrl: " + dbUrl);
        System.out.println("dbFile: " + dbFile);
        ReloadMdb reload = new ReloadMdb();
        if (dbUrl != null && !"".equals(dbUrl.trim())) {
            reload.reload(dbUrl);
            MesgBox msg = new MesgBox("Reload Successfully!");
            vecSuccess.add(msg);
        } else if (dbFile != null) {
            String tmpDir = System.getProperty("java.io.tmpdir");
            File unZipDir = FileUtil.getFile(tmpDir + "/db-mdb");
            if (unZipDir.exists()) {
                FileUtil.delete(unZipDir.getAbsolutePath());
            }
            System.out.println("unzip file ...");
            AppZipManager zipMgr = new AppZipManager();
            zipMgr.unZipIt(dbFile.getAbsolutePath(), unZipDir.getAbsolutePath());
            List<String> list = new ArrayList<String>();
            List<String> pattern = new ArrayList<String>();
            pattern.add(".mdb");
            FileUtil.generateFileList(unZipDir, list, pattern);
            if (list.size() > 0) {
                File mdb = FileUtil.getFile(unZipDir + "/" + list.get(0));
                if (mdb == null) {
                    throw new Exception("data mdb not found!");
                }
                System.out.println("unzip file size: " + mdb.getTotalSpace());
                reload.reload(mdb);
                MesgBox msg = new MesgBox("Reload Successfully!");
                vecSuccess.add(msg);
            } else {
                throw new Exception("data mdb not found!");
            }
        } 
        if (dbUrl == null && dbFile == null) {
            System.out.println("reload start to execute ...");
            reload.execute(null);
            MesgBox msg = new MesgBox("Reload Successfully!");
            vecSuccess.add(msg);
        }
        return SUCCESS;
    
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
    }

    // to prevent the file is inside reply content
    //public File getDbFile() {
    //    return dbFile;
    //}

    public void setDbFile(File dbFile) {
        this.dbFile = dbFile;
    }

}