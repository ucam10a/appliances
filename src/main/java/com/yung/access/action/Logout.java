package com.yung.access.action;

import java.util.Map;

import com.yung.access.transaction.IDataManager;
import com.yung.access.transaction.IQueryManager;

public class Logout extends AbstractAction {

    /**
     * 
     */
    private static final long serialVersionUID = -7172267208708699023L;
    
    @Override
    protected boolean requireAuthenticate() {
        return false;
    }

    @Override
    protected String executeAction(IQueryManager querymgr, IDataManager datamgr, @SuppressWarnings("rawtypes") Map session) throws Exception {
        
        session.remove("login");
        return SUCCESS;
    
    }
    
}
