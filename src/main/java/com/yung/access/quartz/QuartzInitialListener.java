package com.yung.access.quartz;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Listener for quartz to start and stop
 * 
 * @author Yung Long Li
 *
 */
public class QuartzInitialListener implements ServletContextListener {

    public void contextDestroyed(ServletContextEvent arg0) {
        //QuartzApp app = new QuartzApp();
        //app.stopQuartz();
    }

    public void contextInitialized(ServletContextEvent arg0) {
        //QuartzApp app = new QuartzApp();
        //app.runQuartz();
    }

}