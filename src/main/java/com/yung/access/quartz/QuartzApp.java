package com.yung.access.quartz;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.AbstractTrigger;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QuartzApp {

    private static final Logger logger = LoggerFactory.getLogger(QuartzApp.class);

    /**
     * run quartz job
     */
    public void runQuartz() {

        Scheduler scheduler = null;
        try {

            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            scheduler.start();

            // define the job and tie it to our HelloJob class
            JobDetail reloadMdb = new JobDetailImpl();
            ((JobDetailImpl) reloadMdb).setName("reconnect");
            ((JobDetailImpl) reloadMdb).setGroup("group1");
            ((JobDetailImpl) reloadMdb).setJobClass(ReloadMdb.class);
            
            AbstractTrigger<?> trigger1 = new CronTriggerImpl();
            trigger1.setName("trigger1");
            trigger1.setGroup("group1");
            trigger1.setJobKey(reloadMdb.getKey());
            ((CronTriggerImpl) trigger1).setCronExpression("0 59 23 * * ?");
            
            // Tell quartz to schedule the job using our trigger
            scheduler.scheduleJob(reloadMdb, trigger1);
            
            logger.info("quartz start! ");

        } catch (Exception e) {
            logger.error(e.toString(), e);
            e.printStackTrace();
        } finally {
            if (scheduler != null) {
                // try {
                // scheduler.shutdown();
                // logger.info("quartz shutdown! ");
                // } catch (SchedulerException e) {
                // e.printStackTrace();
                // }
            }
        }

    }

    /**
     * stop quartz job
     */
    public void stopQuartz() {

        Scheduler scheduler = null;

        try {

            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.getDefaultScheduler();

            // and start it off
            if (scheduler != null) scheduler.shutdown(false);

        } catch (SchedulerException e) {
            logger.error("quartz fail!", e);
            e.printStackTrace();
        }

    }
    
}
