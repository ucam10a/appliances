package com.yung.access.quartz;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.yung.access.util.AppZipManager;
import com.yung.access.util.ToolConfig;
import com.yung.debug.FileUtil;

public class ReloadMdb implements Job {

    public static String DBURL = "https://docs.google.com/uc?id=1pT196kuewi-yaiuIz44r8MtkgVxqrJJn&export=download";
    
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        reload(DBURL);
    }

    public void reload(String dbUrl) throws JobExecutionException {
        try {
            String copyZipPath = FileUtil.getFile(ToolConfig.getServerHome() + "/COMPANY.zip").getAbsolutePath();
            deleteFile(copyZipPath);
            File zip = FileUtil.getFile(copyZipPath);
            System.out.println("reload zip ...");
            URL url = new URL(dbUrl);
            FileUtils.copyURLToFile(url, zip);
            System.out.println("reload zip done!");
            unzipMdb(copyZipPath);
        } catch (Exception e) {
            throw new JobExecutionException(e);
        }
    }
    
    private void unzipMdb(String zipFilePath) throws Exception {
        AppZipManager zipmgr = new AppZipManager();
        String zipFolder = ToolConfig.getServerHome() + "/COMPANY";
        FileUtil.delete(zipFolder);
        zipmgr.unZipIt(zipFilePath, zipFolder);
        List<String> fileList = new ArrayList<String>();
        List<String> patterns = new ArrayList<String>();
        patterns.add(".mdb");
        FileUtil.generateFileList(FileUtil.getFile(zipFolder), fileList, patterns);
        File mdbFile = FileUtil.getFile(zipFolder + "/" + fileList.get(0));
        FileUtil.fileCopy(mdbFile, ToolConfig.getServerHome() + "/db.mdb");
        FileUtil.delete(zipFolder);
    }
    
    public void load() throws JobExecutionException {
        try {
            String copyZipPath = FileUtil.getFile(ToolConfig.getServerHome() + "/COMPANY.zip").getAbsolutePath();
            File zip = FileUtil.getFile(copyZipPath);
            if (!zip.exists()) {
                System.out.println("load zip ...");
                URL url = new URL(DBURL);
                FileUtils.copyURLToFile(url, zip);
                System.out.println("load zip done!");
                unzipMdb(copyZipPath);
            }
        } catch (Exception e) {
            throw new JobExecutionException(e);
        }
    }
    
    public void loadMdb() throws JobExecutionException {
        try {
            String copyMdbPath = FileUtil.getFile(ToolConfig.getServerHome() + "/db.mdb").getAbsolutePath();
            File mdb = FileUtil.getFile(copyMdbPath);
            if (!mdb.exists()) {
                System.out.println("load zip ...");
                URL url = new URL(DBURL);
                String copyZipPath = FileUtil.getFile(ToolConfig.getServerHome() + "/COMPANY.zip").getAbsolutePath();
                File zip = FileUtil.getFile(copyZipPath);
                FileUtils.copyURLToFile(url, zip);
                System.out.println("load zip done!");
                unzipMdb(copyZipPath);
            }
        } catch (Exception e) {
            throw new JobExecutionException(e);
        }
    }
    
    private void deleteFile(String path) {
        File copyMdb = FileUtil.getFile(path);
        if (copyMdb.exists()) {
            copyMdb.delete();
        }
    }

    public void reload(File dbFile) throws JobExecutionException {
        try {
            String copyMdbPath = FileUtil.getFile(ToolConfig.getServerHome() + "/db.mdb").getAbsolutePath();
            deleteFile(copyMdbPath);
            System.out.println("delete old mdb done!");
            FileUtil.fileCopy(dbFile, copyMdbPath);
            System.out.println("reload mdb file done!");
            unzipMdb(copyMdbPath);
        } catch (Exception e) {
            throw new JobExecutionException(e);
        }
    }
    
}