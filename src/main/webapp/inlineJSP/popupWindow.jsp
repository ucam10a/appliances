<!-- The popup window for edit comment Dialog result -->
<div id="editCommentDialog" title="<s:property value="chMsgMap['comment']" /><s:property value="chMsgMap['edit']" />" style="overflow-x:auto; overflow-y:auto;">
    <div>
        <textarea id="comment-editor-comment" cols="85" rows="17" ></textarea>
        <input id="comment-editor-fundId" type="hidden" value="" />
		<br><br>
        <input type="button" value="<s:property value="chMsgMap['submit']" />" onclick="submitFundComment()" />
    </div>
</div>
<script type="text/javascript" language="JavaScript">
$("#editCommentDialog").dialog({
    autoOpen: false,
    height: 500,
    width: 850,
    modal: true,
    buttons: {
    	\u53D6\u6D88 : function() {
            $( this ).dialog( "close" );
        }
    },
    close: function() {
    }
});
</script>
<!-- The popup window for delete fund Dialog result -->
<div id="sellFundDialog" title="<s:property value="chMsgMap['sell.fund']" />" style="overflow-x:auto; overflow-y:auto;">
    <table>
        <tr>
        <td>
            <h2><s:property value="chMsgMap['buy.fund.info']" /> - <span id="sellFund-name"></span></h2>
            <table border=1 >
                <tr>
                    <td><s:property value="chMsgMap['fund.price']" /></td>
                    <td><span id="sellFund-price" ></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['fund.unit']" /></td>
                    <td><span id="sellFund-unit"></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['buy.date']" /></td>
                    <td><span id="sellFund-buyDate"></span></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['buy.exchange']" /></td>
                    <td><span id="sellFund-exchange" ></span></td>
                </tr>
            </table>
        </td>
        <td>
            <br>
            <h2> &nbsp; </h2>
            <table>
                <tr>
                    <td><s:property value="chMsgMap['sell.price']" /></td>
                    <td><input id='sell-fund-price' type="text" size=10 value='' /></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['sell.unit']" /> <span id="max-sell-fund-unit"></span></td>
                    <td><input id='sell-fund-unit' type="text" size=10 value='' /></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['sell.date']" /></td>
                    <td><input id='sell-fund-sellDate' name="fundId" type="text" size=10 value='' /></td>
                </tr>
                <tr>
                    <td><s:property value="chMsgMap['sell.exchange']" /></td>
                    <td><input id='sell-fund-exchange' name="fundId" type="text" size=10 value='' /></td>
                </tr>
                <tr><td></td><td align="right"><input type="button" value='<s:property value="chMsgMap['submit']" />' onclick="sellFund();" /></td></tr>
            </table>
            <input id='sell-fund-id' name="fundId" type="hidden" value='' />
        </td>
        </tr>
    </table>
    
</div>
<script type="text/javascript" language="JavaScript">
$("#sellFundDialog").dialog({
    autoOpen: false,
    height: 500,
    width: 850,
    modal: true,
    buttons: {
        \u53D6\u6D88 : function() {
            $( this ).dialog( "close" );
        }
    },
    close: function() {
    }
});
</script>