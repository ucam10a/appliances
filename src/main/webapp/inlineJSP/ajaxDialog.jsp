<link rel="stylesheet" href="themes/base/ui.all.css">
<script src="external/jquery.bgiframe-2.1.2.js"></script>
<script src="ui/ui.core.js"></script>
<script src="ui/ui.widget.js"></script>
<script src="ui/ui.mouse.js"></script>
<script src="ui/ui.button.js"></script>
<script src="ui/ui.draggable.js"></script>
<script src="ui/ui.position.js"></script>
<script src="ui/ui.resizable.js"></script>
<script src="ui/ui.dialog.js"></script>
<script src="ui/effects.core.js"></script>
<script src="ui/ui.autocomplete.js"></script>