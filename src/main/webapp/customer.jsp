<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta content='user-scalable=no,initial-scale=1,maximum-scale=1' name='viewport' />
<title>千允電器</title>
<%@ include file="inlineJSP/headElements.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
<script>
    
    
    
</script>
</head>
<body>
<div id="main" style="max-width: 800px; margin: 0 auto; position: relative;">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="customer-div">
        <div id="content-div"></div>
    </div>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
</body>
</html>