<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Message Page</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<%@ include file="inlineJSP/headElements.jsp" %>
</head>
<body>

<div id="main">
    <!-- content begins -->
    <table align="center" ><tr><td>
    	<div id="content">
            <table height="300">
                <tr valign="top"><td align="center"> 
                <h2><s:property value="錯誤" /></h2>
                <s:iterator value="vecSuccess" id="MesgBox">
                    <b><font size="+1" color="#23456B"><s:property value="message" /><br/><br/></font></b>
                </s:iterator>
                <s:iterator value="vecError" id="MesgBox">
                    <b><font size="+1" color="#FF0000"><s:property value="message" /><br/><br/></font></b>
                </s:iterator>
                </td></tr>
            </table>
        </div>
        <!-- content ends -->
    </td></tr></table>
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
</body>
</html>