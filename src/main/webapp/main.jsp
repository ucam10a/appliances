<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta content='user-scalable=no,initial-scale=1,maximum-scale=1' name='viewport' />
<title>千允電器</title>
<%@ include file="inlineJSP/headElements.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>
<div id="main" style="max-width: 800px; margin: 0 auto; position: relative;">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="content" style="font-size: 12px;">
        <table align="center" width="95%" >
            <tr>
                <td align="center"><b>
                    <s:form id="searchForm" action="showMain" theme="simple" method="POST">
                    	<label for="text1" style="font-size: 20px;" >姓名</label>
                    	<input id="name" type="text" name="name" value="" data-clear-btn="true" />
                    	<label for="text1" style="font-size: 20px;" >電話</label>
                    	<input id="phone" type="text" name="phone" value="" data-clear-btn="true" />
                    	<label for="text1" style="font-size: 20px;" >地址</label>
                    	<input id="name" type="text" name="address" value="" data-clear-btn="true" />
                    	<input id="submit-right" type="submit" value="搜尋" />
                	</s:form>
                </b></td>
            </tr>
        </table>
        <br><br>
        <table align="center" width="95%">
        	<tr>
        		<td>
        			<s:iterator value="customers" id="customer">
        				<div data-role="collapsible" >
			        		<h4><s:property value="name" /> &nbsp; <s:property value="address" /></h4>
			        		<table align="center" width="95%" border="1px" >
					            <tr>
					                <td>客戶姓名</td>
					                <td>客戶電話</td>
					                <td>客戶手機</td>
					                <td>客戶地址</td>
					                <td>分類</td>
					            </tr>
					            <tr>
					                <td><s:property value="name" /></td>
					                <td><s:property value="phone" /></td>
					                <td><s:property value="cellPhone" /></td>
					                <td><s:property value="address" /></td>
					                <td><s:property value="level" /></td>
					            </tr>
					        </table>
					        <br>
					        <table align="center" width="95%" border="1px" >
					            <tr>
					                <td><s:property value="comment" /></td>
					            </tr>
					        </table>
					        <br>
						    <table align="center" width="95%" border="1px">
						        <tr>
						            <td>購買機型</td>
						            <td>售出日期</td>
						            <td>售出金額</td>
						            <td>備註</td>
						            <td>付款方式</td>
						        </tr>
						        <s:iterator value="solds" id="sold">
						            <tr>
						                <td><s:property value="type" /></td>
						                <td><s:property value="soldDateStr" /></td>
						                <td><s:property value="price" /></td>
						                <td><s:property value="comment" /></td>
						                <td><s:property value="payment" /></td>
						            </tr>
						        </s:iterator>
						    </table>
						    <br>
						    <table align="center" width="95%" border="1px">
						        <tr>
						            <td>機型</td>
						            <td>服務內容</td>
						            <td>服務日期</td>
						            <td>服務金額</td>
						            <td>備註</td>
						        </tr>
						        <s:iterator value="maintenances" id="maintenance">
						            <tr>
						                <td><s:property value="type" /></td>
						                <td><s:property value="task" /></td>
						                <td><s:property value="serviceDateStr" /></td>
						                <td><s:property value="price" /></td>
						                <td><s:property value="comment" /></td>
						            </tr>
						        </s:iterator>
						    </table>
             	        </div>
                    </s:iterator>
                </td>	
     	    </tr>
        </table>
    </div>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
</body>
</html>