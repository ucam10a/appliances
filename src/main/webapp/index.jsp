<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta content='user-scalable=no,initial-scale=1,maximum-scale=1' name='viewport' />
<title>千允電器</title>
<%@ include file="inlineJSP/headElements.jsp" %>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>
<div id="main">
    <!-- header begins -->
    <jsp:directive.include file="inlineJSP/header.jsp"/>
    <jsp:directive.include file="inlineJSP/navigation.jsp"/>
    <!-- content begins -->
    <div id="content" style="width: 95%">
        <table align="center">
            <tr><td>
            <s:if test="#session.login != 'true'">
                <h2><span id="title" >請輸入密碼</span></h2>
                <s:form action="login" theme="simple" method="POST">
                    <input type="password" name="password" /><br/><br/>
                    <input id="submit" type="submit" value="送出" />
               </s:form>
            </s:if>
            </td></tr>
        </table>
    </div>
    <!-- content ends -->
</div>
<jsp:directive.include file="inlineJSP/footer.jsp"/>
</body>
</html>